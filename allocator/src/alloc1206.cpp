//
// Demonstrates the relational operations 
// of NP_Array class

#include <iostream>
using namespace std;

#include "np_a.h"

// display integer
void display(int v)
{
	cout << v << " ";
}

int main()
{	
	NP_Array <int> ob1(2,3), ob2(2,3), ob3(4,4);
	int i;

	// give ob1 and ob2 some value
	for(i=-3; i<2; i++) {
		ob1[i] = i;
		ob2[i] = i;
	}

	cout << "\nContents of ob1: ";
	for(i=-3; i<2; i++) cout << ob1[i] << " ";
	cout << "Contents of ob2: ";
	for(i=-3; i<2; i++) cout << ob2[i] << " ";
	cout << "\n\n";

	if(ob1 == ob2) cout << "ob1 == ob2\n";
	if(ob1 != ob2) cout << "Error\n";
	cout << endl;

	cout << "Assign ob1[-1] the value 99\n";
	ob1[-1] = 99;
	cout << "Contents of ob1 now is ";
	for(i=-3; i<2; i++) cout << ob1[i] << " ";
	cout  << endl;

	if(ob1 == ob2) cout << "Error\n";
	if(ob1 != ob2) cout << "ob1 != ob2\n";
	cout << endl;

	if(ob1 <  ob2) cout << "ob1 <  ob2\n";
	if(ob1 <= ob2) cout << "ob1 <= ob2\n";
	if(ob1 >  ob2) cout << "ob1 >  ob2\n";
	if(ob1 >= ob2) cout << "ob1 >= ob2\n";

	if(ob2 <  ob1) cout  << "ob2 <  ob1\n";
	if(ob2 <= ob1) cout  << "ob2 <= ob1\n";
	if(ob2 >  ob1) cout  << "ob2 >  ob1\n";
	if(ob2 >= ob1) cout  << "ob2 >= ob1\n";
    cout << endl;
	// compare object of difference size
	if(ob3 != ob1) cout << "ob3 != ob1\n";
	if(ob3 == ob1) cout << "ob3 == ob1\n";
  return 0;
}