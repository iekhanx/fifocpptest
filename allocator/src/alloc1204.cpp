// 
// Demonstrates bitset
//
#include <iostream>
#include <bitset>
using namespace std;

int main()
{
	bitset<64> b(32);
	bitset<64> b2(0);

	cout << "\nOriginal bits: ";
	cout << b << endl;
	
	// assign values.
	b[0]  = 1;
	b[1]  = 1;
	b[10] = 1;
	b[12] = 1;

	cout << "Bit after assignments ";
	cout << b << endl;

	// rotate bits
	b << 2;
	cout << "Bits after left rotate ";
	cout << b << endl;

	// flip bits
	cout << "After flipping bits ";
	b.flip();
	cout << b << endl;

	// see if any bits set
	if(b.any())
		cout << "b has at least 1 bitset.\n";

	//count set bits
	cout << "b has " << b.count();
	cout << "  bits set\n";

	//test bits three different ways
	if(b[0] == 1)
		cout << "bit 0 is on.\n";
	if(b.test(1))
		cout << "bit 1 is on.\n";
	if(b.flip(2) == 1)
		cout << "bit 2 is on.\n";

	// can add bits ot integers
	cout << "Add 11 to bit 0: " << b[0] + 11 << endl;
  return 0;
}
