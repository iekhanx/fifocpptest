/*
	A simplified, custom container that implements 
	an array that allows both positive and negative 
	indexes.

	This file np_a.h
	Date 19/11/2008
	By   Imran Khan
*/

#include <iostream>
#include <iterator>
#include <algorithm>
using namespace std;

template <class T, class Allocator = allocator<T> >

class NP_Array {
	T *c;			// pointer to array of elements.	
	unsigned zero;		// hold location of zero index
	unsigned pos_extent;	// extent in positive direction
	unsigned neg_extent;	// extent in negative direction
	Allocator  a;		// allocator for memory 

public:                         // typedef typename T::X   Type
	typedef T  value_type;
	typedef Allocator allocator_type;
	typedef typename Allocator::reference reference;
	typedef typename Allocator::const_reference const_reference;
	typedef typename Allocator::size_type size_type;
	typedef typename Allocator::difference_type difference_type;
	typedef typename Allocator::pointer pointer;
	typedef typename Allocator::const_pointer const_pointer;

	// forward iterators
	typedef T * iterator;
	typedef const T * const_iterator;

	// This container does not support reverse iterators.
	// but you can add them if you like?

	// constructor and destructor.
	//
	// default constructor
	NP_Array()
	{ 
		pos_extent  = neg_extent = 0;
		zero = 0;
		c = a.allocate(0, 0);
	}

	// construct object of specified dimensions
	NP_Array(unsigned p, unsigned n, const T &t= T())
	{
		c = a.allocate(p+n, 0);
		pos_extent = p;
		neg_extent = n;
		zero = n;
		for(int i=0; i<size(); i++) a.construct(&c[i], t);
	}

	// construct for range
	NP_Array(iterator start, iterator stop)
	{
		c = a.allocate(stop - start, 0);
		pos_extent = stop - start;
		neg_extent = 0;
		zero = 0;
		for(int i=0; i<size(); i++) a.construct(&c[i], *start++);
	}

	// copy constructor
	NP_Array(const NP_Array &o)
	{
		c = a.allocate(o.size(), 0);
		pos_extent = o.pos_extent;
		neg_extent = o.neg_extent;
		zero = o.zero;
		for(int i=0; i<size(); i++)
			c[i] = o.c[i];
	}

	// destructor
	~NP_Array()
	{
		for(int i=0; i<size(); i++) a.destroy(&c[i]);
		a.deallocate(c, size());
	}

	// *********** Operator functions ******************//
	T &operator[](int i)
	{
		return c[zero + i];
	}

	const T &operator[](int i) const
	{
		return c[zero + i];
	}

	NP_Array &operator=(const NP_Array &o)
	{
		a.deallocate(c, size());
		c = a.allocate(o.size(), 0);
		pos_extent = o.pos_extent;
		neg_extent = o.neg_extent;
		zero       = o.zero;
		for(int i=0; i<size(); i++)
			a.construct(&c[i], o.c[i]);
		return *this;
	}

	// *************** insert function *************** //
	iterator insert(iterator p, const T &val)
	{
		iterator q;
		T *tmp = a.allocate(size()+1, 0);
		register int i, j;

		// copy existing elements to new array 
		// inserting new element if possible.
		for(i=j=0; i<size(); i++, j++) {
			if(&c[i] == p) {
				tmp[j] = val;
				j++;
				q = &tmp[j];
			}
			tmp[j] = c[i];
		}
		if(p == end()) { // new element goes on end
			tmp[j] = val;
			q = &tmp[j];
		}

		// adjust zero point as needed
		if(p < &c[zero]) {
			zero++;
			neg_extent++;
		}
		else 
			pos_extent++;

		a.deallocate(c, size());
		c = tmp;
      
	   return q;
	}

	void insert(iterator p, int num, const T &val)
	{
		for(; num>0; num--) p = insert(p, val);
	}

	void insert(iterator p, iterator start, iterator stop)
	{
		while(start != stop) {
			p = insert(p, *start);
			start++;
		}
	}

	// ************* Erase Functions *************//
	iterator erase(iterator p)
	{
		iterator q = p;
		if(p != end()) a.destroy(p);

		// adjust zero point as needed
		if(p < &c[zero]) {
			neg_extent--;
			zero--;
		}
		else
			pos_extent--;
		// compact remaining elements
		for(; p<end(); p++)
			*p = *(p+1);

		return q;
	}

	iterator erase(iterator start, iterator stop)
	{
		iterator p = end();
		int i;

		for(i=stop-start; i>0; i--)
			p = erase(start);

		return p;
	}

	// ************ Push and Pop functions **************//
	void push_back(const T &val)
	{
		insert(end(), val);
	}

	void pop_back()
	{
		erase(end() -1);
	}

	// ***********Iterator Functions ****************//
	iterator begin()
	{
		return &c[0];
	}

	iterator end()
	{
		return &c[pos_extent + neg_extent];
	}

	const_iterator begin() const
	{
		return &c[0];
	}

	const_iterator end() const
	{
		return &c[pos_extent + neg_extent];
	}

	//************* Misc. Functions ****************//
	size_type size() const
	{
		return end() - begin();
	}

	bool empty()
	{
		return size() == 0;
	}

	void swap(NP_Array &b)
	{
		NP_Array<T> tmp;

		tmp = *this;
		*this = b;
		b = tmp;
	}

	void clear()
	{
		for(int i=0; i<size(); i++) {
			a.destroy(&c[i]); }
		pos_extent = neg_extent = 0;
	}

	// return extent
	int get_neg_ext()
	{
		return neg_extent;
	}

	int get_pos_ext()
	{
		return pos_extent;
	}

	//********************* Option to Add *************//
	// for fun, try adding the following features:
	//
	// - reverse iterators
	// - rbegin()  and rend()
	// - the at() function
	// - pop_front() and push_front()

};

	//************* Relational operators ***************//
	template <class T, class Allocator>
	bool operator==(const NP_Array<T, Allocator> &a,
		            const NP_Array<T, Allocator> &b)
	{
		if(a.size() != b.size()) return false;
	}

	template<class T, class Allocator>
	bool operator !=(const NP_Array<T, Allocator> &a,
		             const NP_Array<T, Allocator> &b)
	{
		return !equal(a.begin(), a.end(), b.begin());
	}

	template<class T, class Allocator>
	bool operator <(const NP_Array<T, Allocator> &a,
		            const NP_Array<T, Allocator> &b)
	{
		return lexicographical_compare(a.begin(), a.end(),
			                           b.begin(), b.end());
	}

	template<class T, class Allocator>
	bool operator >(const NP_Array<T, Allocator> &a,
		            const NP_Array<T, Allocator> &b)
	{
		return a < b;
	}

	template<class T, class Allocator>
	bool operator <=(const NP_Array<T, Allocator> &a,
		             const NP_Array<T, Allocator> &b)
	{
		return !(a > b);
	}

	template<class T, class Allocator>
	bool operator >=(const NP_Array<T, Allocator> &a,
		             const NP_Array<T, Allocator> &b)
	{
		return !(b < b);
	}

/*
// A partial container adaptor skeleton

template <class T, class Cont = vector<T> >

class C_Adaptor {
	Cont c;			//store an instance of container being adapted

public:
	typedef typename Cont::value_type               value_type;
	typedef typename Cont::allocator_type           allocator_type;
	typedef typename Cont::reference                reference;
	typedef typename Cont::const_reference          const_reference;
	typedef typename Cont::size_type                size_type;
	typedef typename Cont::difference_type          difference_type;
	typedef typename Cont::iterator                 iterator;
	typedef typename Cont::const_iterator           const_iterator;
	typedef typename Cont::reverse_iterator         reverse_iterator;
	typedef typename Cont::const_reverse_iterator   const_reverse_iterator;

	// some skeletal constructors
	C_Adaptor() { ..
		c = Cont;
	}
	C_Adaptor(int n, const T &t = T()) {
		c = Cont(n, t);
	}

	// a few exampel member functions
	T &operator[](int i) {
		return c[i];
	}

	iterator insert(iterator p, const T &v) {
		return c.insert(p,v);
	}

	iterator begin() {
		return c.begin();
	}

	iterator end() {
		return c.end();
	}

	// fill in the rest...
};
*/
