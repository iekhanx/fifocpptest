//
// Demonstrate array as a container
//
#include <iostream>
#include <list>
#include <algorithm>
using namespace std;

int main()
{
	list <int> lst(10);
	list <int>::iterator p;
	int *ip, *ip_end;
	int num[10] = {0,1,2,3,4,5,6,7,8,9};
	int i;

	cout << "\nInitial contents of num: ";
	for(i=0; i<10; i++)
		cout << num[i] << " ";
	cout << endl;

	// copy nums array to list.
	copy(num, &num[9], lst.begin());

	cout << "Content of lst after copy: ";
	for(p=lst.begin(); p != lst.end(); p++)
		cout << *p << " ";
	cout << endl;

	// remove elements that are less than 5
	ip_end = remove_copy_if(lst.begin(), lst.end(), num, bind2nd(less<int>(), 5));

	cout << "Content of nums after remove_copy_if()\n";
	for(ip = num; ip != ip_end; ip++)
		cout << *ip << " ";
	cout << endl;
  return 0;
}