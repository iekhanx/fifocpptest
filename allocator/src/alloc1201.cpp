//
// Demonstrate allocator max_size() function
//
#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector <short int>::allocator_type si_a;
	vector <int>::allocator_type i_a;
	vector <long int>::allocator_type li_a;
	vector <float>::allocator_type f_a;
	vector <double>::allocator_type d_a;

	cout << "\nHere are number of objects that can be allocated.\n";
	cout << "short integer: ";
	cout << si_a.max_size() << endl;

	cout << "integer: ";
	cout << i_a.max_size() << endl;

	cout << "long integers: ";
	cout << li_a.max_size() << endl;

	cout << "doubles: ";
	cout << d_a.max_size() << endl;
 return 0;
}