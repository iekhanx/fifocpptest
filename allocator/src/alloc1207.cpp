//
// Demonstrate NP_Array class store object.
//
#include <iostream>
using namespace std;
#include "np_a.h"

class test {
public:
	int a;
	test() { cout << "Constructing\n"; a = 0; }
	test(const test &o) { cout << "Copy constructor\n";
	a = o.a;
	}
	~test() { cout << "Destructor called!\n"; }
};

int main()
{
	NP_Array<test> t(2,3);
	int i;

	for(i=-3; i<2; i++) t[i].a = i;
	for(i=-3; i<2; i++) cout << t[i].a << " ";
	cout << endl;

	// copy to new container
	NP_Array<test> t2(4,7);
	//copy(t.begin(), t.end(), &t2[-2].a);

	cout << "Contents of t2:\n";
	for(i=-7; i<4; i++) cout << t2[i].a << " ";
	cout << endl;

	NP_Array<test> t3(t.begin()+1, t.end()-1);
	cout << "Contents of t3:\n";
	for(i=-t3.get_neg_ext(); i<t3.get_pos_ext(); i++)
		cout << t3[i].a << " ";
	cout << endl;

	t.clear();
	cout << "Size after clear(): " << t.size() << endl;

	// assign container objects 
	t = t3;
	cout << "Contents of t:\n";
	for(i=t.get_neg_ext(); i<t.get_pos_ext(); i++)
		cout << t[i].a << " ";
	cout << endl;
  return 0;
}
