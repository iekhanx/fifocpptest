//
// Demmonstrate NP_Array custom container class
//
#include <iostream>
#include <algorithm>
#include <functional>
#include "np_a.h"
using namespace std;

// display integers
void display(int v)
{
	cout << v << " ";
}

int main()
{	
	NP_Array <int> ob(5,5);
	NP_Array <int>::iterator p;
	int i, sum;

	cout << "\nSize of ob is: " << ob.size()<< endl;

	cout << "Initial contents of ob:\n";
	for(i=-5; i<5; i++) cout << ob[i] << " ";
	cout << endl;

	// give ob some value
	for(i=-5; i<5; i++) ob[i] = i;

	cout << "New values for ob:\n";
	p = ob.begin();
	do{
		cout << *p++ << " ";
	}while(p != ob.end());
	cout << endl;

	// display sum of negative indexes
	sum =0;
	for(i= -ob.get_neg_ext(); i<0; i++)
		sum += ob[i];
	cout << "Sum of values with negative subscripts is: ";
	cout << sum << "\n\n";

	// use copy() to copy one object to another
	cout << "Copy ob to ob2 using copy() algorithm.\n";
	NP_Array<int>ob2(ob.get_pos_ext(), ob.get_neg_ext());
	copy(ob.begin(), ob.end(), ob2.begin());

	// use for_each algorithm to display ob2
	cout << "Contents of ob2:\n";
	for_each(ob2.begin(), ob2.end(), display);
	cout << endl;

	// use remove_copy_if() to remove those values less than 0
	cout << "Remove values less than zero and";
	cout << " put result into ob3.\n";
	NP_Array<int> ob3(ob.get_pos_ext(), ob.get_neg_ext());
	replace_copy_if(ob.begin(), ob.end(),ob3.begin(), bind2nd(less<int>(), 0),0);
	cout << "Contents of ob3.\n";
	for_each(ob3.begin(), ob3.end(), display);
	cout << endl; cout << endl;

	cout << "Swap ob and ob3.\n";
	ob.swap(ob3);                  //swap ob and ob3
	cout << "Here is ob3.\n";
	for_each(ob3.begin(), ob3.end(),display);
	cout << endl;
	cout << "Swap again to restore.\n";
	ob.swap(ob3);			      // restore
	cout << "Here is ob3 after second swap:\n";
	for_each(ob3.begin(), ob3.end(), display);
	cout  << endl;

	// use insert() member functions
	cout << "Element at ob[0] is " << ob[0] << endl;
	cout << "Insert values into ob.\n";
	ob.insert(ob.end(), -9999);
	ob.insert(&ob[1], 99);
	ob.insert(&ob[-3], -99);
	for_each(ob.begin(), ob.end(), display);
	cout << endl;
	cout << "Element at ob[0] is  "<< ob[0] << endl;
	cout << endl;

	cout << "Insert -7 three times to front of ob.\n";
	ob.insert(ob.begin(), 3, -7);
	for_each(ob.begin(), ob.end(), display);
	cout << endl;
	cout << "Element at ob[0] is " << ob[0] << endl;
	cout << endl;

	// use push_back() and pop_back()
	cout << "Push back the value 40 onto ob.\n";
	ob.push_back(40);
	for_each(ob.begin(), ob.end(), display);
	cout << endl;
	cout << "Pop back two value from ob.\n";
	ob.pop_back(); ob.pop_back();
	for_each(ob.begin(), ob.end(), display);
	cout << endl; cout << endl;

	// use erase
	cout << "Erase element at 0 \n";
	p = ob.erase(&ob[0]);
	for_each(ob.begin(), ob.end(), display);
	cout << endl;
	cout << "Element at ob[0] is " << ob[0] << endl;
	cout << endl;

	cout << "Erase many elements in ob.\n";
	p = ob.erase(&ob[-2], &ob[3]);
	for_each(ob.begin(), ob.end(), display);
	cout << endl;
	cout << "Element at ob[0] is " << ob[0] << endl;

	cout << "Insert ob4 into ob.\n";
	NP_Array<int> ob4(3, 0);
	for(i=0; i<3; i++) ob4[i] = i+100;
	ob.insert(&ob[0], ob4.begin(), ob4.end());
	for_each(ob.begin(), ob.end(), display);
	cout << endl;
	cout << "Element at ob[0] is " << ob[0] << endl;
	cout << endl;

	cout << "Here is ob shown with its indices:\n";
	for(i=-ob.get_neg_ext(); i<ob.get_pos_ext(); i++)
		cout << "[" << i << "]: " << ob[i] << endl;
	cout << endl;

	cout << "Make a copy of ob2\n";
	NP_Array<int> ob5(ob2);
	for_each(ob5.begin(), ob5.end(), display);
	cout << "\n\n";

	cout << "Clear ob.\n";
	ob.clear();
	for_each(ob.begin(), ob.end(), display);
	cout << "Size of ob after clear(): " << ob.size() << endl;
	cout << endl;cout << endl;

	cout << "Construct object from a range.\n";
	NP_Array<int> ob6(&ob2[-2], ob2.end());
	cout << "Size of ob6: " << ob6.size() << endl;
	for_each(ob6.begin(), ob6.end(), display);
	cout << endl;
  return 0;
}