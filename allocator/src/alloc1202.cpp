// 
//Demonstrate raw_storage_iterator()
//
#include <iostream>
#include <deque>
#include <memory>
#include <algorithm>
using namespace std;

class X {
	int a, b;
	int sum;
public:
	X() { a = b = 0; sum = 0; }
	X(int x, int y) { a = x; b = y; }

	// copy constructor
	X(const X &o) { a = o.a; b = o.b; sum = o.sum; } // assign sum

	// overloaded assignment
	X operator=(const X &o) { a = o.a; b = o.b; return *this; }

	void setsum() { sum = a+b; }
	void show() { cout << a << "," << b; cout << " Sum is: " << sum << endl; }
};

int main()
{
	unsigned char raw1[100], raw2[100];
	X *p;
	deque <X> q(5);
	int i;

	for(i=0; i<5; i++) { 
		q[i] = X(i, i);
		q[i].setsum();
	}
	// store deque in uninitialised memory the wrong way
	copy(q.begin(), q.end(), (X*)raw1);

	cout << "\nContents of raw memory (incorrect)\n";
	p = (X*) raw1;
	for(i=0; i<5; i++) p[i].show();

	// store deque in uninitilised memory  the right way
	copy(q.begin(), q.end(), raw_storage_iterator<X *, X>((X *)raw2));

	cout << "Contents of raw memory (correct)\n";
	p = (X*) raw2;
	for(i=0; i<5; i++) p[i].show();

  return 0;
}
