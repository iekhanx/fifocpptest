#ifndef NDIMH
#define NDIMH
// ndim.h - Multi-dimensional array prototypes
#include <stdarg.h>

void ndim(void *, ...);
void nfree(void *, int);
#endif
