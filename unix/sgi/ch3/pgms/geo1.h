#ifndef GEOMETRYH
#define GEOMETRYH
// geo1.h - geometry functions

struct Point {
	double x, y;
};

double slope(Point, Point);
#endif
