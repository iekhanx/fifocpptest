// Qualifier what it is?
#ifdef _QUALIFIER
// You can use Qualifiers to indicate what size of number you want to store inside your int. Think the exact size varies by implementation of C, but typically it's as follows.

short int a; //  16 bits, range -32,768 to 32,767

unsigned short int b; //  16 bits, range 0 to 65,535

unsigned int c; //  32 bits, range 0 to 4,294,967,295

int d; //  32 bits, range -2,147,483,648 to 2,147,483,647

long int d; //  32 bits, range -2,147,483,648 to 2,147,483,647 (minimum requirement, can be higher on 64bit systems)

#endif

// STATIC  - demonstration
//
#include <stdio.h>

void foo()
{
    int a = 10;
    static int sa = 10;

    a += 5;
    sa += 5;

    printf("a = %d, sa = %d\n", a, sa);
}


int main()
{
    int i;

    for (i = 0; i < 10; ++i)
        foo();
}
#ifdef _VOLITILE

typedef struct
{
  int command;
  int data;
  int isbusy;
} MyHardwareGadget;
Now you want to send some command:

void SendCommand (MyHardwareGadget * gadget, int command, int data)
{
  // wait while the gadget is busy:
     while (gadget->isbusy)
       {
           // do nothing here.
        }
               // set data first:
               gadget->data    = data;
               // writing the command starts the action:
               gadget->command = command;
         }
/* Looks easy, but it can fail because the compiler is free to change the order in which data and commands are written. This would cause our little gadget to issue commands with the previous data-value. Also take a look at the wait while busy loop. That one will be optimized out. The compiler will try to be clever, read the value of isbusy just once and then go into an infinite loop. That's not what you want.

The way to get around this is to declare the pointer gadget as volatile. This way the compiler is forced to do what you wrote. It can't remove the memory assignments, it can't cache variables in registers and it can't change the order of assignments either:
*/

// This is the correct version:
void SendCommand (volatile MyHardwareGadget * gadget, int command, int data)
{
   // wait while the gadget is busy:
   while (gadget->isbusy)
   {
       // do nothing here.
   }
   // set data first:
   gadget->data    = data;
  // writing the command starts the action:
   gadget->command = command;
}
#endif

