// Demonstrate unique()
#include <iostream>
#include <list>
using namespace std;

int main()
{
	list<int> lst;
	list<int>::iterator p;

	for(int i=0; i<5;i++)
		for(int j=0; j<3; j++) lst.push_back(i);

	cout << "\nOriginal list :";
	for(p=lst.begin(); p!=lst.end(); p++)
		cout << *p << " ";
	cout << "\n\n";

	// remove consecutive duplicates
	lst.unique();

	cout << "Modified list: ";
	for(p=lst.begin(); p!=lst.end(); p++)
		cout << *p << " ";
	cout << "\n\n";
  return 0;
}	
