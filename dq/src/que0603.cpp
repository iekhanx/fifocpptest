// Demonstrate a priority_queue class
#include <iostream>
#include <queue>
using namespace std;

int main()
{
	priority_queue<int> q;

	q.push(1);
	q.push(3);
	q.push(2);
	q.push(4);
	q.push(5);

	while(!q.empty()) {
		cout << "\nPoping " << q.top() << "\n";
		q.pop();
	}
 return 0;
}