// Compute average and standard deviation
// of a set of data
#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int main()
{
	vector<double> v;
	int i;
	double sum=0.0, avg=0.0, std_dev=0.0;

	cout << "\nEnter numbers (0 to stop):\n";
	do{ cout << ": "; cin >> i; if(i) v.push_back(i); 
	}while(i); cout << "\n";

	// find average
	for(i=0; i<v.size(); i++) 
		sum += v[i]; 
	avg = sum / v.size(); 
	cout << "Average is: " << avg << "\n\n";
	
	// compute standard deviation
	sum = 0.0;
	for(i=0; i<v.size(); i++)
		sum += (v[i] - avg) * (v[i] - avg);
	std_dev = sqrt(sum / v.size());
	cout << "Standard deviation is: " << std_dev; 
	cout << "\n";
  return 0;
}