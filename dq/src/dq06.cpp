// Assigning deque objects
#include <iostream>
#include <deque>

using namespace std;
int main()
{
	deque<char> q1(10), q2;
	int i;

	for(i=0; i<10; i++) q1[i] = i + 'A';

	cout << "Contents of q1 are: ";
	for(i=0; i<q1.size(); i++) cout << q1[i]; cout << "\n\n";

	// assign one deque to another
	q2 = q1;

	cout << "Size of q2 is " << q2.size() << "\n";
	cout << "Contents of q2 are: ";
	for(i=0; i<q2.size(); i++) cout << q2[i]; cout << "\n\n";

	// assign is range from q1 to q2
	q2.assign(q1.begin() + 2, q1.end()-2);

	cout << "Size of q2 is " << q2.size() << "\n";
	cout << "Contents of q2 are: ";
	for(i=0; i<q2.size(); i++) cout << q2[i]; cout << "\n";
	
	// assign elements to q2
	q2.assign(8, 'X');
	cout << "Size of the q2 is " << q2.size() << "\n";
	cout << "Contents of q2 are: ";
	for(i=0; i<q2.size(); i++) cout << q2[i];

  return 0;
}