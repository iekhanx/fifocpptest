// Demonstrate the queue class

#include <iostream>
#include <queue>
#include <string>
using namespace std;
int main()
{
	queue<string> q;

	cout << "\nPusing one two three four...\n\n";
	q.push("one");
	q.push("two");
	q.push("three");
	q.push("four");
	q.push("five");
	while(!q.empty()) {
		cout << "Poping out ";
		cout << q.front() << "\n";
		q.pop();
	}
 return 0;
}