// Demonstrate resize() function


#include <iostream>
#include <deque>
#include <algorithm>
#include <cmath> 
// const double pi = 3.14159265358979323846
using namespace std;
int op_increase (int i) { return ++i; }

int main()
{
	deque<double> radians, sines;
	double num;
	int i;

	for(;;) { cout << "\nEnter angles (0 to stop): ";
	cin >> num;
	if(num == 0.0) { cout << "\n"; break;}
	radians.push_back(num);
	}

	// resize sines so that it can hold all of the results
	sines.resize(radians.size());

	transform(radians.begin(), radians.end(), sines.begin(), op_increase);

	for(i=0; i<sines.size(); i++) { 
		cout << "Angle: "; cout << radians[i] << "\tsines: " << sines[i];
	cout << "\n"; }
	cout << "\n";
  return 0;
}

/*
int main()
{
  deque<double> radians, sines;
  double n;
  unsigned int i;

  radians.push_back(0.1);
  radians.push_back(0.2);
  radians.push_back(0.3);
  radians.push_back(0.4);
  radians.push_back(0.5);

  sines.resize(radians.size());
  transform(radians.begin(), radians.end(), sines.begin(), sin);
  for (i = 0; i < sines.size(); i++)
    cout << radians[i] << ":" << sines[i] << endl;
  return 0;
}
*/
