// A vector may allocate more memory that it currently needs.
#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<char> v(10);

	cout << "\nInitial size:     " << v.size() << "\n";
	cout << "Inital  capacity: " << v.capacity();
	cout << "\n\n";

	v.push_back('X');	// push another element

	cout << "Size after push_back():       " << v.size() << "\n";
	cout << "New Capacity after push_back: " << v.capacity();
	cout << "\n\n";

	v.resize(100);     // resize upto 100
	
	cout << "Size after resize:     " << v.size() << "\n";
	cout << "Capacity after resize: " << v.capacity();
	cout << "\n\n";

	v.push_back('Y');   // push another element onto v

	cout << "Size after push_back():         " << v.size() << "\n";
	cout << "New Capacity after push_back(): " << v.capacity();
	cout << "\n\n";

 return 0;
}
