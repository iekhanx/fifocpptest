// Cycling through a queue.
#include <iostream>
#include <queue>
#include <string>
using namespace std;

int main()
{
	queue<string> q;

	q.push("one");
	q.push("two");
	q.push("three");
	q.push("four");
	q.push("five");

	cout << "\nContent of queue: ";
	for(int i=0; i<q.size(); i++) {
		cout << q.front() << " ";
		// remove front and push on back
		q.push(q.front());
		q.pop();
	}
	cout << "\n\n";

	cout << "Now remove elements:\n";
	while(!q.empty()) {
		cout << "Poping out: ";
		cout << q.front() << "\n";
		q.pop();
	}
 return 0;
}