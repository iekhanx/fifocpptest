// A four-function postfix calculator.
#include <iostream>
#include <stack>
#include <string>
#include <cmath>
using namespace std;

int main()
{
	stack<double> st;
	double a, b;
	string s;

	do {
		cout << ": ";
		cin >> s;

		switch(s[0]) 
		{
		case 'q': // quite calculator
			break;
		case '.':  // show top of stack
			cout << st.top() << "\n";
			break;
		case '+':  // add
			if(st.size() < 2) { cout << "\nOperand missing\n"; 	break; 	}
			a = st.top();
			st.pop();
			b = st.top();
			st.pop();
			cout << a + b << "\n";
			st.push(a+b);
			break;
		case '-':       // substract
			// see if user entering a negative number?
			if(s.size() != 1) { 
				// push the value onto the stack
				st.push(atof(s.c_str()));
				break;
			}
			// otherwise substract the value
			if(st.size() < 2) { cout << "Operand missing\n"; break; }
			a = st.top();
			st.pop();
			b = st.top();
			st.pop();
			cout << b-a << "\n";
			st.push(b-a);
			break;
		case '*':     // Multiply
			if(st.size() < 2) { cout << "Operand missing\n"; break; }
			a = st.top();
			st.pop();
			b =st.top();
			st.pop();
			cout << a*b << "\n";
			st.push(a*b);
			break;
		case '/':       // divide
			if(st.size() < 2) { cout << "Operand missing\n"; break; }
			a = st.top();
			st.pop();
			b = st.top();
			cout << b/a << "\n";
			st.push(b/a);
			break;
		default:
			// push value oto the stack
			st.push(atof(s.c_str()));
			break;
		}
	}while(s != "q");
  return 0;
}
