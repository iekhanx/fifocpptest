// Storing class object in set class
#include <iostream>
#include <set>
#include <string>
using namespace std;

// This class stores states and their capitals.
class state 
{
	string name;
	string cap;
public:
	state() { name = cap = "";}
	// construct temporary object using only state name
	state(string s) { name = s; cap = "" ; }

	// construct an entire state object
	state(string n, string c) { name = n; cap = c; }

	string get_name() { return name; }
	string get_cap()  { return cap;  }
};

// overloaded function
// Compare objects using name of state.
bool operator<(state a, state b)
{
	return a.get_name() < b.get_name();
}

// Create an inserter for state
ostream &operator<<(ostream &s, state &o)
{
	s << o.get_name() << "'s capital is ";
	s << o.get_cap()  << "." << endl;
  return s;
}

int main()
{
	set <state> s;

	// initialise the set 
	s.insert(state("Illinois", "Springfield"));
	s.insert(state("Wisconsin", "Madison"));
	s.insert(state("Missuri", "Jefferson City"));
	s.insert(state("California", "Sacramento"));
	s.insert(state("Navada", "Carson City"));
	s.insert(state("Arkansas", "Little Rock"));
	s.insert(state("Alaska", "Juneau"));
	s.insert(state("West Virginia", "Charleston"));

	// display contents of set
	set <state>::iterator p = s.begin();

	cout << "\n";
	do{
		//cout << s;
		p++;
	} while(p != s.end());
	cout << endl;

	// find a specific state
	cout << "Looking for Wisconsin.\n";
	p = s.find(state("Wisconsin"));
	if(p != s.end()) {
		//cout << "Found.\n" << s;
	}
	else 
		cout << "State not found in list\n";
  return 0;
}


