// Using a list to store mailling addresses.
#include <iostream>
#include <list>
#include <string>
using namespace std;

class maillist
{
	string name;
	string street;
	string city;
	string state;
	string zip;

public:
	maillist() { name = street = city = state = zip = " "; }
	maillist(string n, string s, string c, string st, string z) {
		name = n; street = s; city = c; state = st; zip = z; }
	string getname(){ return name;}
	string getcity(){ return city;}
	string getstreet(){ return street;}
	string getstate() { return state;}
	string getzip() { return zip;}
};

// overloaded function mandatory
// list sorted by name.
bool operator<(maillist &a, maillist &b)
{
	return a.getname() < b.getname();
}

// list searched by name.
bool operator==(maillist &a, maillist &b)
{
	return a.getname() == b.getname();
}

// display list on screen
void display(list<maillist> &lst)
{
	list<maillist>::iterator p;

	for(p=lst.begin(); p!=lst.end(); p++) {
		cout << p->getname()   << ":  ";
		cout << p->getstreet() << ":  ";
		cout << p->getstate()  << ":  ";
		cout << p->getzip()    << ":  \n";
	}
}

int main()
{
	list<maillist> mlsta, mlstb;

	mlsta.push_back(maillist("James, Top","1102 W. Hanry St", "Mission", "TX","785772"));
	mlsta.push_back(maillist("Newton, Sid","55 Oscar Blvd", "Kirksvile", "MO", "65301"));
	mlsta.push_back(maillist("Henson Erick", "908 Trunk Ave.","Peoria", "IL", "61615"));
	mlsta.push_back(maillist("Ewen Heidi", "43654 N. Broadway #4", "Idaho Falls", "ID", "83401"));
	mlsta.push_back(maillist("Mount W.C.","78A Wothington Rd", "Berkeley","CA","94710"));

	mlstb.push_back(maillist("Williams, Don", "197 NorthRidge Dr","Walworth", "WI","53184"));
	mlstb.push_back(maillist("Newton John", "56 Oscar Blvd", "Kirksvile","MO", "63501"));
	mlstb.push_back(maillist("Ewen George", "43658 S. Broadway", "Idaho Falls", "ID","89647"));
	mlstb.push_back(maillist("Weston George", "5464 Woodbury Ct", "Baker", "LA", "70714"));
	// some duplicates
    mlstb.push_back(maillist("Mount W.C.","78A Wothington Rd", "Berkeley","CA","94710"));   
	mlstb.push_back(maillist("Ewen Heidi", "43654 N. Broadway #4", "Idaho Falls", "ID", "83401"));

	// sort the list
	mlsta.sort();

	mlstb.sort();

	// merge mailing lists
	mlsta.merge(mlstb);
	cout << "\nList A after sorting and merging.\n";
	display(mlsta);
	cout << "\nList A now has " << mlsta.size();
	cout << " entries.\n\n";

	// remove duplicate
	mlsta.unique();
	cout << "List A after removing duplicates.\n";
	display(mlsta);
	cout << "\nList A now has " << mlsta.size();
	cout << " entries.\n\n";
  return 0;
}
