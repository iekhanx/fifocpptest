// Capacity vs Size
#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<char> v(10);

	cout << "\nInitial size:     " << v.size() << "\n";
	cout << "Initial Capacity: " << v.capacity();
	cout << "\n\n";

	v.reserve(20);    // reserve more space (memory)
	cout << "Size after reserve:     " << v.size() << "\n";
	cout << "Capacity after reserve: " << v.capacity();
	cout << "\n\n";
  return 0;
}