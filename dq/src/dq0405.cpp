// [] vs at() function
#include <iostream>
#include <stdexcept>      // std::out_of_range
#include <vector>
using namespace std;

int main()
{
	vector<int> v(10);
	int i;

	try{
		for(i=0; i<100; i++) v.at(i) = 100;
	}
	catch(const out_of_range &excpt) {
		cout << "\nOut-of-bounds access attempted ";
		cout << "at location " << i << " " << excpt.what() << "\n";
	}
	// this will cause a program crash
	for(i=0; i<100; i++) v[i] = 100;
  return 0;
}
