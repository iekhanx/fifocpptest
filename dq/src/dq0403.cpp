// Use reserve() to avoid reallocation.
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

const int getsize =0;
const int getbytes =1;
const int reset =2;

int producer(int what);
void consumer(vector<int> &v);

int main()
{
	char more;
	vector<int> v;

	do {
		consumer(v);

		cout << "Current contents of v:\n";
		for(int i=0; i<v.size(); i++) { cout << v[i] << " ";
		if(!((i+1)%16)) cout << "\n";
		}
		cout << "\nMore ? (Y/N): ";
		cin >> more;
	} while(more == 'y');
   return 0;
}

// producer data
int producer(int what)
{
	static int howmany = rand() % 20 + 1;

	switch(what) {
		case getsize: return howmany; break;
		case getbytes: return howmany--; break;
		case reset: howmany = rand() %20 + 1; return howmany;
	}
 return 0;   // error
}

// Read data and put o n the end of v
void consumer(vector<int> &v)
{
	int packet_size = producer(getsize);

	cout << "\nProducer sending " << packet_size << " unit.\n";

	v.reserve(packet_size + v.size());
	do { v.push_back(producer(getbytes));
	packet_size--;
	} while(packet_size);
  producer(reset);
}