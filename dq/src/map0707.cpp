// Map can store only unique keys.
#include <iostream>
#include <map>
using namespace std;

int main()
{
	map<char, int> m;
	pair<map<char,int>::iterator, bool> res;

	// insert A
	 res = m.insert(pair<char, int>('A', 65));
	 if(res.second) cout << "\nInsertion occurred.\n";

	 // try to insert A again
	 res = m.insert(pair<char, int>('A', 99));
	 if (!res.second) cout << "\nDuplicate not allowed\n";
	 
	 map<char, int>::iterator p;
	 p = m.find('A');
	 cout << "Its ASCII value is " << p->second << endl;
   return 0;
}

