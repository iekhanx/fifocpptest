// A Splicing example
#include <iostream>
#include <list>
#include <string>
#include <algorithm>
using namespace std;

int main()
{
	list<string> sentence;
	list<string> phrase;
	list<string>::iterator p;
	string s1[] = { "occur", "at", "" };
	string s2[] = { "Splicing", "can", ""};
	string s3[] = { "or", "the", "end.", ""};
	string s4[] = { "the", "front,", "the", "middle,", ""};
	int i;

	// construct initial sentence
	for(i=0; s1[i]!= ""; i++) sentence.push_back(s1[i]);

	// construct 1st phrase to add
	for(i=0; s2[i]!=""; i++) phrase.push_back(s2[i]);

	cout << "\nOriginal sentence: ";
	p = sentence.begin();
	while(p!=sentence.end())
		cout << *p++ << " ";
	cout << "\n\n";

	// splice at the front
	sentence.splice(sentence.begin(), phrase);

	cout << "Sentence after splicing at the front:\n";
	p = sentence.begin();
	while(p!=sentence.end())
		cout << *p++ << " ";
	cout << "\n\n";

	// construct next phrase
	for(i=0; s3[i]!=""; i++)
		phrase.push_back(s3[i]);

	// splice at end.
	sentence.splice(sentence.end(), phrase);

	cout << "Sentence after splicing at the end:\n";
	p = sentence.begin();
	while(p!=sentence.end())
		cout << *p++ << " ";
	cout << "\n\n";

	// construct final phrase
	for(i=0; s4[i]!=""; i++)
		phrase.push_back(s4[i]);

	// splice in the phrase before the "or"
	p = find(sentence.begin(), sentence.end(), "or");
	sentence.splice(p, phrase);

	cout << "Sentence afte splicing in the middle:\n";
	p = sentence.begin();
	while(p!=sentence.end())
		cout << *p++ << " ";
	cout << "\n\n";
  return 0;
}