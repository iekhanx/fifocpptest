// Using different comparison function.
#include <iostream>
#include <queue>
#include <functional>
using namespace std;

int main()
{
	priority_queue<int, vector<int>, greater<int> > q;

	q.push(1);
	q.push(3);
	q.push(0);
	q.push(2);
	q.push(4);
	q.push(7);

	while(!q.empty()) {
		cout << "\nPoping " << q.top() << "\n";
		q.pop();
	}
 return 0;
}