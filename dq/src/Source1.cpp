#include <queue>
#include <vector>

struct adult{void feed(){}};
typedef std::pair<adult*,double> my_pair;

std::pair<adult*,double> my_make_pair(adult* t, double  t1)
{
	return std::make_pair<adult*,double>(t,t1);
}

struct my_pair_less
{
  bool operator() (my_pair const& lhs, my_pair const& rhs)
  {
	return lhs.second < rhs.second;
  }
};


int main(int /*argc*/, char ** /*argv*/ )
{
	
	typedef std::priority_queue<my_pair,std::vector<my_pair>,my_pair_less> Container;

	adult Jimmy;
	adult Billy;
	adult Sammy;
	adult Jill;

	Container c;
	c.push(my_make_pair(&Jimmy,2.0));
	c.push(my_make_pair(&Billy,17.0));
	c.push(my_make_pair(&Sammy,0.02));
	c.push(my_make_pair(&Jill,20.4));
	c.top().first->feed();//Jill is now fed
	c.pop();
	c.top().first->feed();//Billy is now fed
	c.pop();
	c.top().first->feed();//Jimmy is now fed
  return 0;
}



