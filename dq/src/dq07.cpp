// demonstrate swap()
#include <iostream>
#include <deque>
using namespace std;

int main()
{
	deque<char> q1, q2;
	int i;

	for(i=0; i<26; i++) q1.push_back(i + 'A');
	for(i=0; i<10; i++) q2.push_front(i + '0');

	cout << "\nSize of q1 and q2: ";
	cout << q1.size() << "  " << q2.size() << "\n";

	cout << "q1 ";
	for(i=0; i<q1.size(); i++) cout << q1[i]; cout << "\n";

	cout << "q2 ";
	for(i=0; i<q2.size(); i++) cout << q2[i]; cout << "\n\n";

	// swap deques using memeber function
	q1.swap(q2);

	cout << "Size of q1 and q2 after first swap: ";
	cout << q1.size() << "  " << q2.size() << "\n";

	cout << "q1 after first swap: ";
	for(i=0; i<q1.size(); i++) cout << q1[i]; cout << "\n";

	cout << "q2 after first swap ";
	for(i=0; i<q2.size(); i++) cout << q2[i]; cout << "\n\n";

	// swap deaues using algorithm
	swap(q1, q2);

	cout << "Size of q1 and q2 after second swap: ";
	cout << q1.size() << "  " << q2.size() << "\n";

	cout << "q1 after second swap: ";
	for(i=0; i<q1.size(); i++) cout << q1[i]; cout << "\n";

	cout << "q2 after second swap: ";
	for(i=0; i<q2.size(); i++) cout << q2[i]; cout << "\n";

  return 0;
}
