// Using reverse() to create a palindrome tester.
#include <iostream>
#include <list>
using namespace std;

char phrase[][80] = {
	"Madam, I 'm Adam.",
	"Able was I ere I saw Elba.",
	"A man, a plan, a canal: Panama",
	"This is not one.",
	"cat and dog",
	"this is a test for reverse characters",
	"Jane Austen's house",
	""
};

int main()
{
	list<char> pal;
	list<char>::iterator p;
	int i, j;

	for(i=0; *phrase[i]; i++) {
		for(j=0; phrase[i] [j]; j++)
			pal.push_back(phrase[i][j]);
	cout << "\nPhrase # " << i << " forward       : ";
	p = pal.begin();
	while(p!=pal.end()) cout << *p++; cout << "\n";

	// remove txtraneous characters
	pal.remove(',');
	pal.remove('.');
	pal.remove('!');
	pal.remove(':');
	pal.remove('\'');
	//pal.remove(' ');

	cout << "Phrase # " << i << " after deletion: ";
	p = pal.begin();
	while(p!=pal.end()) cout << *p++; cout << "\n";

	// reverse the list
	pal.reverse();
	
	cout << "Phrase # " << i << " backwrd       : ";
	p=pal.begin();
	while(p!=pal.end()) cout << *p++; cout << "\n\n";
	
	// get ready for next phrase
	pal.clear();
	}
  return 0;
}