// Merging won't work if the list aren't
#include <iostream>
#include <list>
using namespace std;

int main()
{
	list<int> lst1, lst2;
	int i;

	lst1.push_back(2);
	lst1.push_back(0);
	lst1.push_back(8);
	lst1.push_back(4);
	lst1.push_back(6);

	for(i=1; i<11; i+=2)
		lst2.push_back(i);

	cout << "Orginal contents of lst1:\n";
	list<int>::iterator p = lst1.begin();
	while(p!=lst1.end())
		cout << *p++ << " ";
	cout << "\n\n";

	cout << "Original contents of lst2:\n";
	p = lst2.begin();
	while(p!=lst2.end())
		cout << *p++ << " ";
	cout << "\n\n";

	// this merge will fail.
	lst1.merge(lst2);

	cout << "Contents of lst1 after failed merge\n";
	p = lst1.begin();
	while(p!=lst1.end())
		cout << *p++ <<  " ";
	cout << "\n\n";
  return 0;
}