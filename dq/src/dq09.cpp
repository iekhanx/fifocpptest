// Watchout iteration value may change.
#include <iostream>
#include <deque>
using namespace std;

int main()
{
	deque<char> q;
	deque<char>::iterator p1, p2;
	int i;

	for(i=0; i<5; i++) q.push_back(i + 'A');

	cout << "\nOriginal sequence: ";
	for(i=0; i<q.size(); i++) cout << q[i] << " "; cout << "\n";

	p1 = q.begin() + 2;
	p2 = q.begin() + 3;
	cout  << "*p1 : " << *p1 << ", ";
	cout  << "*p2 : " << *p2 << "\n";
	cout  << "\n";

	// now insert an element into the sequence
	q.insert(p1, 'X');

	cout << "Sequence after insert: ";
	for(i=0; i<q.size(); i++) cout << q[i] <<  " "; cout << "\n";

	// these now points to different elements
	cout  << "*p1 : " << *p1 << ", ";
	cout  << "*p2 : " << *p2 << "\n";
	cout  << "\n";

  return 0;
}