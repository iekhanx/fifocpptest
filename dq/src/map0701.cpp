// A simple map demonstration.
#include <iostream>
#include <map>
using namespace std;

int main()
{
	map<char, int> m;
	int i;

	// put pairs into map
	for(i=0; i<26; i++) { m.insert(pair<char, int>('A' + i, 65+i)); }

	char ch;
	cout << "\nEnter key: ";
	cin >> ch;

	// convert to upper case
	ch = toupper(ch);

	map<char, int>::iterator p;

	// find the value of the given key
	p = m.find(ch);
	if(p != m.end())
		cout << "Its ASCII value is " << p->second;
	else
		cout << "Key not in map\n";
	cout << "\n";
  return 0;
}