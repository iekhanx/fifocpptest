// Reverse iterators and copy
#include <iostream>
#include <deque>
#include <algorithm>
#include <cstring>
using namespace std;

int main()
{
	deque <char> q1(30), q2, q3;
	int i;

	char str1[] = "forward";
	for(i=0; str1[i]; i++) q2.push_back(str1[i]);

	// copy in forward direction
	copy(q2.begin(), q2.end(), q1.begin());

	cout  << "Contents q1 after forward copy\n";
	for(i=0; i<q1.size(); i++) { cout << q1[i]; }

	cout << "\n\n";

	char str2[] = "backward";
	for(i=0; str2[i]; i++) q3.push_back(str2[i]);

	// copy in reverse direction
	copy(q3.rbegin(), q3.rend(), q1.begin()+ strlen(str1));

	cout << "Contents q1 after reverse copy:\n";
	for(i=0; i<q1.size(); i++) cout << q1[i];

  return 0;
}