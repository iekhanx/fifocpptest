// Using clear() function
#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector <int> v;
	int i;

	for(i=0; i<10; i++) v.push_back(i);

	cout << "\nInitial contents of v: ";
	for(i=0; i<v.size(); i++) cout << v[i] << " "; cout << "\n";

	cout << "Initial size of v:     " << v.size() << "\n";
	cout << "Initial capacity of v: " << v.capacity() << "\n\n";

	v.clear();      // using clear() function

	cout << "Size of v after clear():     " << v.size() << "\n";
	cout << "Capacity of v after clear(): " << v.capacity() << "\n\n";

	// put new value in to v
	for(i=0; i<10; i++) v.push_back(i*10);
	
	cout << "New contents of v: ";
	for(i=0; i<v.size(); i++) cout << v[i] << "  "; cout << "\n";

	cout << "Size of v after adding new elements: ";
	cout << v.size() << "\n";

	cout << "Capacity of v after adding new element: ";
	cout << v.capacity() << "\n\n";
  return 0;
}
