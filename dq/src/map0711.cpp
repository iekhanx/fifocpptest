// Use a multimap to improve the phone directory.
#include <iostream>
#include <map>
#include <string>
using namespace std;

// This is a key class
class name {
	string str;
public:
	name() { str = ""; }
	name(string s) { str = s; }
	string get() { return str; }
};

// Must define less than relative to name objects.
bool operator<(name a, name b)
{
	return a.get() < b.get();
}

// This is the value class.
class phonenum {
	string str;
public:
	phonenum() { str = ""; }
	phonenum(string s) { str = s; }
	string get() { return str; }
};

int main()
{
	multimap<name, phonenum> d;

	// put names and numbers into map.
	d.insert(pair<name, phonenum>(name("Tom"), phonenum("554-3435")));
	d.insert(pair<name, phonenum>(name("Tom"), phonenum("232-9845")));
	d.insert(pair<name, phonenum>(name("Chris"),phonenum("653-4820")));
	d.insert(pair<name, phonenum>(name("John"), phonenum("125-3837")));
	d.insert(pair<name, phonenum>(name("Rachel"),phonenum("111-3343")));
	d.insert(pair<name, phonenum>(name("Rachel"),phonenum("111-3223")));
	d.insert(pair<name, phonenum>(name("Rachel"),phonenum("1211-6663")));

	// given a name find number
	string s;
	cout << "\nEnter name: ";
	cin >> s;

	multimap<name, phonenum>::iterator p;

	p = d.find(s);
	if(p != d.end()) {
		do { cout << "Phone number: " << p->second.get();
		cout << endl;
		p++;
		}while(p != d.upper_bound(s));
	}
	else
		cout << "Name is not in the directory\n";
  return 0;
}