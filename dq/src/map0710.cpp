// Demonstrates a multimap
#include <iostream>
#include <map>
#include <string>
using namespace std;

int main()
{
	multimap<string, string> n;
	string s;
	
	n.insert(pair<string, string>("Jones", "Fred"));
	n.insert(pair<string, string>("Jones", "Alice"));
	n.insert(pair<string, string>("Smith", "Tom"));
	n.insert(pair<string, string>("Smith", "Alicia"));
	n.insert(pair<string, string>("Smith", "Jon"));
	n.insert(pair<string, string>("Doe",   "Harvey"));
	n.insert(pair<string, string>("Doe",   "Wilma"));
	n.insert(pair<string, string>("Doe",   "Rachel"));
	n.insert(pair<string, string>("Johnson","J.T"));

	multimap<string, string>::iterator p;
	cout << "\nEnter last name: ";
	cin  >> s;

	// get iterator to first occurrence
	p = n.find(s);
	if(p != n.end()) {
		do {
			cout << s << ", " << p->second << endl;
			p++;
		}while(p != n.upper_bound(s));
	}
	else
		cout << "Name not found.\n";
  return 0;
}	
