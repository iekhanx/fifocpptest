// A simple stack example.
#include <iostream>
#include <stack>
using namespace std;

int main()
{
	stack<char> st;
	
	st.push('A');
	st.push('B');
	st.push('C');
	st.push('D');

	while(!st.empty()) {
		cout << "\nPopping: " << st.top() << "\n";
		st.pop();
	}
  return 0;
}	