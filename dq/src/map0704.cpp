// Automatically insert elements.
#include <iostream>
#include <map>
using namespace std;

#ifdef _BETTER_ 
int main()
{
	map <char, int > m;

	cout << "\nInitial size of map: " << m.size() << endl;

	// find value given key
	cout << "The value associated with A is: " << m['A'];
	cout << endl;
	cout << "Size of map is now: " << m.size() << endl;
  return 0;
}
#else
	int main()
	{
		map <char, int> m;
		int i;

		// put the value into pair using [ ]
		for(i=0; i<26; i++) m['A'+i] = 65+i;

		char ch;
		cout << "Enter key: ";
		cin >> ch;

		// find value given key
		cout << "Its ASCII vlaue is: " << m[ch];
     return 0;
	}
#endif