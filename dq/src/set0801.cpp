// A simple example that uses set
#include <iostream>
#include <set>
#include <string>
using namespace std;

int main()
{
	set <string> s;

	// create a set of strings
	s.insert("Telescope");
	s.insert("House");
	s.insert("Computer");
	s.insert("Mouse");
	s.insert("Keyboard");

	set <string>::iterator p = s.begin();
	cout << "\n";
	do {
		cout << *p << " ";
		p++;
	}while(p != s.end());
	cout << endl;

	p = s.find("Telescope");
	if(p != s.end())
		cout << "Found: " << *p << endl;
 return 0;
}
