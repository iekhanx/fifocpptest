// Using a multiset to record 
// responses to the polling question.

#include <iostream>
#include <set>
using namespace std;

enum re { strong_agree, agree, disagree,
                strong_disagree, no_option };

int main()
{
	multiset<re> q;
	
	// create some responses
	q.insert(re(strong_agree));
	q.insert(re(agree));
	q.insert(re(strong_agree));
	q.insert(re(strong_disagree));
	q.insert(re(disagree));
	q.insert(re(disagree));
	q.insert(re(no_option));
	q.insert(re(agree));
	q.insert(re(strong_agree));

	// display result
	cout << "\nRespondent that strongly agree: " << q.count(strong_agree) << endl;
	cout << "Respondent that agree: " << q.count(agree) << endl;
	cout << "Respondent that disagree: " << q.count(disagree) << endl;
	cout << "Respondent that strongly disagree: " << q.count(disagree) << endl;
	cout << "Respondent with no option: " << q.count(no_option) << endl;
  return 0;
}
