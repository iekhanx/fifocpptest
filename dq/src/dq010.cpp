// Using a deque to reassemble out-of-order informatino packets.
#include <iostream>
#include <deque>
#include <algorithm>
#include <string>
using namespace std;

class packet {
	int pnum;		// packet number
	string info;	// information
public:
	packet() { pnum = -1; info = "";}
	packet(int n, string i) { pnum = n; info = i; }
	int getpnum() const { return pnum; }
	string getinfo() { return info; }
};

// Compare two packets for less than
bool operator<(const packet &a, const packet &b) { return a.getpnum() < b.getpnum(); }

// Compare two packet for equality
bool operator==(const packet &a, const packet &b) { return a.getpnum() == b.getpnum(); }

// Obtain the next packet return true if packet available and false otherwise.
bool getpacket(packet &pkt)
{
	static packet message[] = {
		packet(2, "is"),
		packet(4, "Programming"),
		packet(0, "The"),
		packet(3, "Power"),
		packet(1, "STL"),
		packet(-1, "") // end of trasmission mark
	};
	static int i=0;
	if(message[i] == packet(-1, "")) return false;
	pkt = message[i];
	cout << "\nSending: "<< pkt.getinfo() << "\n";

	i++;
	return true;
}

int main()
{
	deque<packet> pack;
	deque<packet>::iterator p;
	packet pkt;

	getpacket(pkt);        // get first packet
	pack.push_back(pkt);   // put first packet in deque
    
	// read and store remaining packets
	while(getpacket(pkt)) {
		if(pkt.getpnum() <= pack.front().getpnum())
			pack.push_front(pkt);
		else if(pkt.getpnum() >= pack.back().getpnum())
			pack.push_back(pkt);
		else {
			p = lower_bound(pack.begin(), pack.end(), pkt);
			pack.insert(p, pkt);
		}
	}
	cout << "\nPackets reassembled:\n";
	for(int i=0; i<pack.size(); i++) {
		cout << pack[i].getinfo() << " "; }
 return 0;
}
