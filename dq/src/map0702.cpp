// Cycle through map using an iterator.

#include <iostream>
#include <map>
using namespace std;

#ifdef _FORWARD_
int main()
{
	map<char, int> m;
	int i;

	// put pair into map
	for(i=0; i<26; i++) m.insert(pair<char, int>('A'+i, 65+i));

	map<char, int>::iterator p;

	cout << "\n";
	// display contents of map
	for(p = m.begin(); p != m.end(); p++)
	{
		cout  << p->first << " has ASCII value of  " << p->second << endl;
	}
 return 0;
}

#else
// Cycle through map using reverse_iterator.
int main()
{
	map<char, int> m;
	int i;

	// put pair into map
	for(i=0; i<26; i++) m.insert(pair<char, int>('A'+i, 65+i));

	map<char, int>::reverse_iterator p;

	cout << "\n";
	// display contents of map
	for(p = m.rbegin(); p != m.rend(); p++)
	{
		cout  << p->first << " has ASCII value of  " << p->second << endl;
	}
 return 0;
}

#endif
