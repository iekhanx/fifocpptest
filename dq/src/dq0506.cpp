// Demonstrate remove()
#include <iostream>
#include <list>
using namespace std;

int main()
{
	list<int>lst;
	list<int>::iterator p;
	int i;

	for(i=0; i<20; i++) lst.push_back(i%3);

	cout << "\nOriginal list: ";
	for(p=lst.begin(); p!=lst.end(); p++)
		cout << *p << " ";

	cout << "\n\n";

	lst.remove(1);  // remove all 1s from the list.

	cout << "Modified list: ";
	for(p=lst.begin(); p!=lst.end(); p++)
		cout << *p << " ";

	cout << "\n\n";
  return 0;
}