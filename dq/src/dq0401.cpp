// Demonstrate the four way that cectors can be created.
#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector<char> a;
	vector<char> b(5, 'X');      // size and initialise
	vector<char> c(b);			 // initialise with another vector
	int i;

	// give the value A through E
	for(i=0; i<5; i++) a.push_back('A' + i);

	// create vector from the range
	vector<char> d(a.begin()+1, a.end() -1);

	// display contents of each
	cout << "\n";
	for(i=0; i<a.size(); i++) cout << "a[" << i << "]: " << a[i] << "  " ;
	cout << "\n\n";

	for(i=0; i<b.size(); i++) cout << "b[" << i << "]: " << b[i] << "  ";
	cout << "\n\n";

	for(i=0; i<c.size(); i++) cout << "c[" << i << "]: " << c[i] << "  ";
	cout << "\n\n";

	for(i=0; i<d.size(); i++) cout << "d[" << i << "]: " << d[i] << "  ";
	cout << "\n\n";
 return 0;
}