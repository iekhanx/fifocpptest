// Demonstrate  merge()
#include <iostream>
#include <list>
using namespace std;

int main()
{
	list<int> lst1, lst2;
	int i;

	for(i=0; i<10; i+=2) lst1.push_back(i);   // add elements lst1

	for(i=1; i<11; i+=2) lst2.push_back(i);   // add elements lst2
	
	cout << "\nOriginal size of lst1: ";
	cout << lst1.size() << "\n";
	cout << "Original contents of lst1:\n";
	list<int>::iterator p = lst1.begin();
	while(p!=lst1.end())
	  cout << *p++ << " ";
	cout << "\n\n";

	cout << "Original size of lst2: ";
	cout << lst2.size() << "\n";
	cout << "Original contents of lst2:\n";
	p = lst2.begin();
	while(p!=lst2.end())
		cout << *p++ << " ";
	cout << "\n\n";

	// merge the two lists
	lst1.merge(lst2);

	cout << "Size of lst1 after merge: ";
	cout << lst1.size() << "\n";
	cout << "Merged contents of lst1\n";
	p = lst1.begin();
	while(p!=lst1.end())
		cout << *p++ << " ";
	cout << "\n\n";

	cout << "lst2 is empty now, its size is ";
	cout << lst2.size() << "\n";
 return 0;
}