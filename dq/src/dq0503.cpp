// Merge into descending order.
#include <iostream>
#include <list>
using namespace std;

int main()
{
	list<int> lst1, lst2;
	int i;

	for(i=10; i>=0; i-=2) lst1.push_back(i);
	for(i=11; i>=1; i-=2) lst2.push_back(i);

	cout << "\nOriginal contents of lst1:  ";
		list<int>::iterator p = lst1.begin();
	while(p!=lst1.end())
		cout << *p++ << " ";
	cout << "\n\n";

	cout << "Original contents of lst2:  ";
	p = lst2.begin();
	while(p!=lst2.end())
		cout << *p++ << " ";
	cout << "\n\n";

	// merge using greater than not less than 
	lst1.merge(lst2, greater<int>());

	cout << "Merged contents of lst1: ";
	p = lst1.begin();
	while(p!=lst1.end())
		cout << *p++ << " ";
	cout << "\n\n";
  return 0;
}