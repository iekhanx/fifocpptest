// Use the greater function object
#include <iostream>
#include <map>
#include <functional>
#include <string>
using namespace std;

int main()
{
	map <string, int, greater<string> > m;

	m["Alpha" ] = 20;
	m["Beta" ] = 19;
	m["Gamma"] = 10;

	map<string, int, greater<string> >::iterator p;
     cout << "\n";
	//  display contents of map
	for(p=m.begin(); p != m.end(); p++) {
		cout << p->first << "  has value of " << p->second << endl;
	}
 return 0;
}