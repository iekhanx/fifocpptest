// Storing class object in a multiset container
// IK 

#include <iostream>
#include <set>
#include <string>
using namespace std;


#define __TEST__   0x0001

// this class store info about book, index by author.
class book 
{
	string author;
	string title;
	string publisher;
	string date;
public:
	book() { title = author = publisher = date = ""; }
	// construct temporary object using only author(i.e. key)
	book(string a) { author = a; title = publisher = date = ""; }
	// construct a complete book object
	book(string t, string a, string p, string d) {
		title = t;
		author = a;
		publisher = p;
		date = d;
	}
	// return only author's name
	string get_author() {return author; }
	// obtain all information about a book
	void get_info(string &t, string &a, string &p, string &d)
	{
		t = title; a = author; p = publisher; d = date;
	}
};

// compare objects using author.
bool operator<(book a, book b)
{
	return a.get_author() < b.get_author();
}

// create inserter for book
ostream &operator<<(ostream &s, book &o)
{
	string t, a, p, d;

	o.get_info(t, a, p, d);
	
	s << a << endl;
	s << t << endl;
	s << p << endl;
	s << d << endl;

  return s;
}

#ifdef __TEST__	
int main()
{
	multiset <book> b;
	string s;

	// initialise the multiset
	b.insert(book("C++ : The complete Reference", 
		"Schildt", "Osborne/McGraw-Hill", "1998"));
	b.insert(book("C++ : From the ground Up", 
		"Schildt", "Osborne/McGraw-Hill", "1998"));
	b.insert(book("C++ : Teach yourself C++", 
		"Schildt", "Osborne/McGraw-Hill", "1997"));
	b.insert(book("Executive Orders", "Clancy", "Putnam", "1996"));
	b.insert(book("The Hunt for Red October", "Clancy","Berkeley", "1985"));
	b.insert(book("Red Strom Raising", "Clancy","Berkeley","1987"));
	b.insert(book("Sphere","Crichton","Balentine","1987"));
	b.insert(book("Jurassic Park","Crichton","Balentine","1990"));
	b.insert(book("Lost World","Crichton", "Balentine", "1995"));

	// display content of set
	multiset<book>::iterator p = b.begin();
	do {
		cout << *p << endl;
		p++;
	}while(p != b.end());
	cout << endl;

	//find a specific book
	cout << "Enter author's name: ";
	cin >> s;
	p = b.find(book(s));
	if(p != b.end()) {
		do { cout << *p << endl; p++; }while(p!=b.upper_bound(s)); }
  return 0;
}
#endif
