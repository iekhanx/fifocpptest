// Using [] operator
#include <iostream>
#include <map>
using namespace std;

int main()
{
	map <char, int> m;
	int i;

	// put pairs into map
	for(i=0; i<26; i++) m.insert(pair<char, int>('A'+i, 65+1));

	char ch;
	cout << "\nEnter key: ";
	cin >> ch;

	//ch = toupper(ch);     // slight enhancement
	// find value given key
	cout << "Its ASCII value is " << m[ch];
  return 0;
}
