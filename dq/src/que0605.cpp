// Storing class object in a priority_queue container.
#include <iostream>
#include <queue>
#include <string>
using namespace std;

// A simple even organiser.
class event {
	int priority;
	string name;
public:
	event() { name = ""; priority =0; }
	event(string n, int p) { name = n; priority = p;}
	string getname() const { return name; }
	int getpriority() const { return priority; }
};

// determine priority
bool operator<(const event &a, const event &b) {
	return a.getpriority() < b.getpriority(); 
}

int main()
{
	priority_queue<event> q;

	q.push(event("Fire!", 10));
	q.push(event("Mail Arrives", 2));
	q.push(event("Phone ring", 3));
	q.push(event("Knock on the door", 4));

	// show priority
	cout << "\nPriorities: ";
	while(!q.empty()) {
		cout << q.top().getname() << "\n";
		cout << "        \n";
		q.pop();
	}
  return 0;
}