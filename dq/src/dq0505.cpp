// Sort a list
#include <iostream>
#include <list>
#include <cstdlib>

using namespace std;

int main()
{
	list <int> lst;
	int i;

	// create a list of random integers.
	for(i=0; i<10; i++) lst.push_back(rand());

	cout << "\nOriginal contents:\n";
	list<int>::iterator p = lst.begin();
	while(p!=lst.end()) {
		cout << *p << " ";
	p++; }
	cout << "\n\n";

	// sort the list in ascending order
	//lst.sort();

	// sort the list descending order
	lst.sort(greater<int>());
	
	cout << "Sorted contents:\n";
	p = lst.begin();
	while(p!=lst.end()) {
		cout << *p << " ";
		p++;
	}
	cout << "\n\n";
  return 0;
}