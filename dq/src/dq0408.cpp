// Store a class opject in a vector
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

class DailyOutput
{
	int units;
public:
	DailyOutput() { units =0; }
	DailyOutput(int x) { units = x;}

	double get_units() { return units; }
};

bool operator<(DailyOutput a, DailyOutput b)
{
	return a.get_units() < b.get_units();
}

bool operator==(DailyOutput a, DailyOutput b)
{ 
	return a.get_units() == b.get_units();
}

int main()
{
	vector <DailyOutput> v;
	vector <DailyOutput>::iterator p;
	int i;

	for(i=0; i<14; i++) v.push_back(DailyOutput(60 + rand()%10));

	cout << "\nDaily production:\n";
	for(i=0; i<v.size(); i++) cout << v[i].get_units() << "  ";
	cout << "\n\n";

	p = min_element(v.begin(), v.end());
	cout << "Minimum units: " << p->get_units();
	cout << "\n";

	p = max_element(v.begin(), v.end());
	cout << "Maximum units: " << p->get_units();
	cout << "\n\n";

	// look for consecutive days with the same production
	p = v.begin();
	do {
		p = adjacent_find(p, v.end());
		if(p != v.end()) {
			cout << "Two consecutive days with ";
			cout << p->get_units() << " units.\n";
			p++;  // move on to next element
		   }
		}while(p != v.end());
  return 0;
}
