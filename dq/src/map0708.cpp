// Use a map to create a phone directory.
// Use a map to store class
#include <iostream>
#include <map>
#include <string>
using namespace std;

// this is key class
class name{
	string str;
public:
	name() { str = ""; }
	name(string s) { str = s;}
	string get() { return str; }
};

// Must define less than relative to name objects.
bool operator<(name a, name b)
{
	return a.get() < b.get();
}

// This is a value class
class phonenum {
	string str;
public:
	phonenum() { str = ""; }
	phonenum(string s) { str = s; }
	string get() { return str; }
};

int main()
{
	map<name, phonenum > d;

	// put name and number into map
	d.insert(pair<name, phonenum>(name("Tom"), phonenum("454-34343")));
	d.insert(pair<name, phonenum>(name("Chris"), phonenum("232-98764")));
	d.insert(pair<name, phonenum>(name("John"), phonenum("787-88339")));
	d.insert(pair<name, phonenum>(name("Rachel"), phonenum("544-8763")));

	// given a name find number
	string str;
	cout << "\nEnter name: ";
	cin >> str;

	map<name, phonenum>::iterator p;

	p = d.find(name(str));

	if(p!=d.end())
		cout << "\nPhone Number: " << p->second.get() << endl;
	else
		cout << "\nName not in the directory\n";
  return 0;
}