// Using copy() function

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	vector<char> v(10);
	vector<char> v2(10);
	int i;

	for(i=0; i<10; i++) v[i] = 'A' + i;
	for(i=0; i<10; i++) v2[i] = 'z';

	// copy all of v int v2
	copy(v.begin(), v.end(), v2.begin());
	cout << "\nContents of v2 after complete copy:\n";
	for(i=0; i<10; i++) cout << v2[i] << " ";
	cout << "\n\n";

	// reinitialise v2
	for(i=0; i<10; i++) v2[i] = 'z';

	// now copy just part of v into v2
	copy(v.begin()+2, v.end()-2, v2.begin());
	cout << "Contents of v2 after subsequence copy:\n";
	for(i=0; i<10; i++) cout << v2[i] << " ";
	cout << "\n\n";

    // reinitialise v2
	for(i=0; i<10; i++) v2[i] = 'z';
	
	// now coppy part of v into middle of v2
	copy(v.begin()+2, v.end()-2, v2.begin()+3);
	cout << "Contents of v2 after copy into middle:\n";
	for(i=0; i<10; i++) cout << v2[i] << " ";
	cout << "\n\n";
  return 0;
}
