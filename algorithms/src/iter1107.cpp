//
// Demonstrate istream_iterator with copy algorithm
//
#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
using  namespace std;

int main()
{	
	int i;
	vector <int> v(5);

	cout << "\nEnter 5 integers:\n";
	istream_iterator<int> int_itr(cin);
	copy(int_itr, istream_iterator<int>(), v.begin());

	cout << "Here are the values you entered: ";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
    cout << endl;
  return 0;
}