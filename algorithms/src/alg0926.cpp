//
// Demonstrates for_each()
//
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	vector <int> v;
	int i;

	for(i=0; i<10; i++) v.push_back(i);

	cout << "\nContents of v: ";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << "\n\n";
return 0;
}
