//
// Demonstrate unique() algorithm
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <char> v;
	vector <char>::iterator p, p_end;
	int i;

	for(i=0; i<5; i++) {
		v.push_back('A' + i);
		v.push_back('A' + i);
		v.push_back('A' + i);
	}
	cout << "\nOriginal content of v:\n";
	for(p=v.begin(); p<v.end(); p++)
		cout << *p << " ";
	cout << endl;

	// remove all C's 
	p_end = unique(v.begin(), v.end());

	cout << "Sequnece after removing duplicates\n";
	for(p=v.begin(); p<p_end; p++)
		cout << *p << " ";
	cout << endl;

  return 0;
}