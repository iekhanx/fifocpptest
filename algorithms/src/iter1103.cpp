//
// Demonstrates back_insert_iterator
//
#include <iostream>
#include <iterator>
#include <vector>
using namespace std;

int main()
{
	vector<int> v, v2;
	vector<int>::iterator p;
	int i;

	for(i=0; i<5; i++)  v.push_back(i);

	cout << "\nOriginal contents of v: ";
	p = v.begin();
	while(p != v.end()) cout << *p++ << " ";
	cout << endl;

	// create back_insert_iterator to v
	back_insert_iterator<vector<int> > bck_i_itr(v);

	// insert rather than overwrite
	*bck_i_itr++ = 500;
	*bck_i_itr = 800;
	cout << "v after insertion ";
	p = v.begin();
	while(p != v.end()) cout << *p++ << " ";
	cout << endl;
	cout << "Size of v2 before copy: " << v2.size() << endl;

	// copy v to v2 using back_inserter
	copy(v.begin(), v.end(),back_inserter(v2));
	cout << "Size of v2 after copy: " << v2.size() << endl;
	cout << "Contents of v2 after insertion ";
	p = v.begin();
	while(p != v.end()) cout << *p++ << " ";
	cout << endl;

  return 0;
}