//
// Demonstrates set algorithms
//  set_union()
//  set_difference(),
//  set_symmetric_difference()
//  set_intersection()
//  includes()
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <char> v1, v2, v_res(26), v3;
	vector <char>::iterator p, pres_end;
	int i;

	for(i=0; i<20; i++) v1.push_back('A'+i);
	for(i=10; i<26; i++) v2.push_back('A'+i);
	
	cout << "\nContents of v1:\n";
	for(i=0;i<v1.size(); i++)
		cout << v1[i];
	cout << endl;

	cout << "\nContents of v2:\n";
	for(i=0;i<v2.size(); i++)
		cout << v2[i];
	cout << endl;
	cout << endl;

	//set_union()
	pres_end = set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), v_res.begin());
	cout << "After using set_union()\n";
	for(p=v_res.begin(); p!=pres_end; p++)
		cout << *p;
	cout << "\n\n";

	// set_difference()
	pres_end = set_difference(v1.begin(), v1.end(), v2.begin(),v2.end(), v_res.begin());
	cout << "After using set_difference()\n";
	for(p=v_res.begin(); p != pres_end; p++)
		cout << *p;
	cout << "\n\n";

	// set_symmetric_difference()
	pres_end = set_symmetric_difference(v1.begin(), v1.end(), v2.begin(), v2.end(), v_res.begin());
	cout << "After using set_symmetric_difference()\n";
	for(p=v_res.begin(); p != pres_end; p++)
		cout << *p;
	cout << "\n\n";

	// set_intersection()
	pres_end = set_intersection(v1.begin(), v1.end(), v2.begin(), v2.end(), v_res.begin());
	cout << "After using set_intersection()\n";
	for(p=v_res.begin(); p != pres_end; p++)
		cout << *p;
	cout << "\n\n";

	// includes()
	v3.push_back('A');
	v3.push_back('C');
	v3.push_back('D');

	if(includes(v1.begin(), v1.end(), v3.begin(), v3.end()))
		cout << "v1 includes all of v3\n";
	else
		cout << "v3 contains element not found in v1\n";

  return 0;
}
