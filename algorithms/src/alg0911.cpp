//
// Demonstrate binary_search() 
//
#include <iostream>
#include <vector>
#include <algorithm>
using  namespace std;

int main()
{
	vector <char> v;
	bool result;
	int i;

	for(i=0; i<10; i++)
		v.push_back('A' + i);

	cout << "\nContents of v: ";
	for(i=0; i<v.size(); i++)
		cout << v[i];
	cout << endl;

	// search for F
	cout << "Looking for F.\n";
	result = binary_search(v.begin(), v.end(), 'F');
	if(result)
		cout << "F Found\n";
	else
		cout  << "F not found\n";

	// search for X
	cout << "Looking for X.\n";
	result = binary_search(v.begin(), v.end(), 'X');
	if(result)
		cout << "X found\n";
	else
		cout << "X not found\n";
return 0;
}
