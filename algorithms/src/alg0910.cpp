// alg0910.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void int_v(vector<int> &v)
{
	v[0] = 99; v[1] = 5;  v[2] = 109; v[3] = -3; 
	v[4] = 44; v[5] = 10; v[6] = 108;
	v[7] = 7;  v[8] = 11; v[9] = 76;
}

int main()
{
	vector <int> v(10);
	int i;

	// sort the entire container;
	int_v(v);    // initialise

	cout << "\nOriginal order of elements in container:\n";
	for(i=0; i<10; i++)
		cout << v[i] << " ";
	cout << endl;

	partial_sort(v.begin(), v.begin()+5, v.end());

	cout << "\nOrder after partial sorting container:\n";
	for(i=0; i<10; i++)
		cout << v[i] << " ";
	cout << endl;
	cout << endl;

	return 0;
}

