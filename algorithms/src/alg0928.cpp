//
// Demonstrate a custom algorithm
// Create Your Own Algorithm (CYOA)
//
#include <iostream>
#include <vector>
#include <algorithm>
#include <list>
using namespace std;

// an algorithm that double the value in specified range.
template<class ForIter> void times2(ForIter start, ForIter end)
{
	while(start != end) {
		*start *=2;
		start++;
	}
}

int main()
{	// apply times2 to a vector of into
	vector <int> v;
	int i;

	for(i=0; i<10; i++) v.push_back(i);

	cout << "\nInitial contents of v:\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << "\n\n";

	times2(v.begin(), v.end());

	cout << "Contents of v after application of times2()\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << "\n\n";

	// apply times2 to list of float
	list <float> lst;
	list <float>::iterator p;

	for(i=0; i<5; i++) lst.push_back((float) i * 3.1416);

	cout << "\nInitial contents of v:\n";
	for(p=lst.begin(); p != lst.end(); p++)
		cout << *p << " ";
	cout << "\n\n";

	times2(lst.begin(), lst.end());

	cout << "\nContents of lst after application of times2()\n";
	for(p=lst.begin(); p != lst.end(); p++)
		cout << *p << " ";
	cout << endl;
  return 0;
}