//
// Demonstrate lower_bound() 
//
#include <iostream>
#include <vector>
#include <algorithm>
using  namespace std;

int main()
{
	vector <char> v;
	vector <char>::iterator p;
	int i;

	for(i=0; i<10; i++)
		v.push_back('A' + i);

	cout << "\nContents of v: ";
	for(i=0; i<v.size(); i++)
		cout << v[i];
	cout << endl;

	// search for F
	cout << "Looking for F.\n";

	p = lower_bound(v.begin(), v.end(), 'F');
	if(p != v.end())
		cout << *p << " Found\n";
	else
		cout  << "F not found\n";

	// search for X
	cout << "Looking for X.\n";
	p = lower_bound(v.begin(), v.end(), 'X');
	if(p != v.end())
		cout << *p << "Found\n";
	else
		cout << "X not found\n";
return 0;
}
