//
// Demmonstrate search()
//
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
using namespace std;

int main()
{
	vector <char> v, v2;
	pair<vector<char>::iterator, vector<char>::iterator> p;
	char str1[] = "One, Two, Three, Two again";
	char str2[] = "One, Two, Four, Five, Nine";
	int i;

	for(i=0; i<(int)strlen(str1); i++)
		v.push_back(str1[i]);

	for(i=0; i<(int)strlen(str2); i++)
		v2.push_back(str2[i]);

	cout << "\nContent of v: ";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i];
	cout << endl;

	cout << "\nContent of v2: ";
	for(i=0; i<(int)v2.size(); i++)
		cout << v2[i];
	cout << endl;

	// find the first two
	p = mismatch(v.begin(), v.end(), v2.begin());

	if(p.first != v.end())
	{
		cout << "\nThe Character " << *p.first;
		cout << " in v mismatches the character ";
		cout << *p.second << " in v2.\n";
	}

	return 0;
}