//
// Demonstrate generate() 
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
// A simple generator function
double f()
{
	static double val = 1.0;
	double t;
	t = 1.0/val;
	val += val;
  return t;
}
int main()
{
	vector <double> v(5);
	int i;

	// generate series
	generate(v.begin(), v.end(), f);

	cout << "\nSeries :";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

  return 0;
}

