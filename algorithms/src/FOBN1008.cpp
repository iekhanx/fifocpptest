//
// Demmonstrate bind2nd() function object binder.
//
#include <iostream>
#include <list>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{
	list<int> lst;
	list <int>::iterator p, endp;
	int i;

	for(i=1; i<20; i++) lst.push_back(i);

	cout << "\nOriginal sequence\n";
	p = lst.begin();
	while(p != lst.end()) { cout << *p << " "; p++; }
	cout << endl;
#ifdef __LESS_THAN__
	endp = remove_if(lst.begin(), lst.end(), bind2nd(greater<int>(), 8));
#else
	endp = remove_if(lst.begin(), lst.end(), bind1st(greater<int>(), 8));
#endif
	cout << "Resulting sequence greter than 8 removed\n";
	p = lst.begin();
	while(p != endp) { cout << *p << " "; p++; }
	cout << endl;
  return 0;
}