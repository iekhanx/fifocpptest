//
// Demonstrates ostream_iterator with copy() algorithm
//
#include <iostream>
#include <list>
#include <iterator>
#include <string>
#include <algorithm>
using namespace std;

int main()
{	
	list <string> ls;
	ostream_iterator<string> o(cout);

	ls.push_back("\nStream ");
	ls.push_back("iterators ");
	ls.push_back("are ");
	ls.push_back("useful.\n");

	copy (ls.begin(), ls.end(), o);
  return 0;
}