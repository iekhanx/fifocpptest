//
// Demonstrate max_element() and min_element()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <int> v(5);
	int i;

	v[0]=100; v[1]=-4; v[2]=55; v[3]=19; v[4]=122;
	
	cout << "\nContents of v:\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	cout << "Maximum element is: ";
	cout << *max_element(v.begin(), v.end());
	cout << endl;

	cout << "Minimum element is: ";
	cout << *min_element(v.begin(), v.end());
	cout << endl;
 return 0;
}
