//
// Demonstrate istreambuf_iterator and
//             ostreambuf_iterator in conjunction
// with algorithm's replace_copy() function.
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>

using namespace std;

int main(int argc, char *argv[])
{
	if(argc != 3) {
		cout << "\nUsage: replace in out.\n";
		return 1;
	}

	ifstream  in(argv[1]);
	ofstream  out(argv[2]);

	// make sure files are open 
	if(!in) {
		cout << "\nCannot open input file.\n";
		return 1;
	}
	if(!out) {
		cout << "\nCannot open output file.\n";
		return 1;
	}

	// create stream_iterator
	istreambuf_iterator<char> in_itr(in);
	ostreambuf_iterator<char> ou_itr(out);

	// Copy file, replacing character in the process 
	replace_copy(in_itr, istreambuf_iterator<char>(), ou_itr, ' ', '|');

	in.close();
	out.close();

  return 0;
}