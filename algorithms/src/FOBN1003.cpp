//
// Demonstrate unary predicate that determine number is even
//
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

bool IsEven(int i)
{ return !(i%2); }

int main()
{
	vector <int> v;
	int i;

	for(i=1; i<20; i++) v.push_back(i);
	
	cout << "\nSequence in v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	i = count_if(v.begin(), v.end(), IsEven);
	cout << i << " number are evenly divisible by 2.\n";
 return 0;
}