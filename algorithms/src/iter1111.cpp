//
// Demonstrate advance() and distance() function
//

#include <iostream>
#include <list>
#include <iterator>
using namespace std;

int main()
{	
	list <char> lst;
	list <char>::iterator p;
	int i;

	for(i=0; i<10; i++) lst.push_back('A' + i);

	p = lst.begin();
	cout << "\nCharacters at p: " << *p << endl;

	// move two character forward using adavance()
	// p += 2;      // this method not allowed to list !!!
	advance(p, 2);  // this is ok
	cout << "Character at p+2 : " << *p << endl;

	// demonstrate distance()
	cout  << "Number of elements from lst.begin() ";
	cout  << "to lst.end().";

	// cout << lst.end() - lst.begin();  // this is not allowed in list!!!
	cout << distance(lst.begin(), lst.end());
	cout << endl;
  return 0;
}