//
// Demonstrate inplace_merge()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <char> v;
	int i;

	for(i=0; i<26; i+=2) v.push_back('a'+i);
	for(i=0; i<26; i+=2) v.push_back('b'+i);

	cout << "\nOriginal content of v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i];
	cout << endl;
	cout << endl;

	// merge two ranges within v
	inplace_merge(v.begin(), v.begin()+13, v.end());

	cout << "Result after inplace_merge():\n";
	for(i=0; i<v.size(); i++)
		cout << v[i];
	cout << endl;

  return 0;
}