//
// Demonstrate transform() sequence
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

double midpoint(double a, double b)
{
	return ((a-b) /2) + b;
}

int main()
{
	vector <double> v1(5), v2(5), v3(5);
	int i;

	v1[0]=10.0; v1[1]=98.6; v1[2]=12.23; v1[3]=88.8; v1[4]=-212.01;
	v2[0]=2.0;  v2[1]=3.3;  v2[2]=4.19;  v2[3]=155.0;v2[4]=-2.0;

	cout << "\nValues in v1:\n";
	for(i=0; i<v1.size(); i++)
		cout << v1[i] << " ";
	cout << endl;

	cout << "Values in v2:\n";
	for(i=0; i<v2.size(); i++)
		cout << v2[i] <<  " ";
	cout << endl;

	// find midpoint between elements in v1 and those in v2
	transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), midpoint);

	// midpoints:
	cout << "Midpoints: ";
	for(i=0; i<v3.size(); i++)
		cout << v3[i] << " ";
	cout << endl;

  return 0;
}