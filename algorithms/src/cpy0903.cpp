// Using swap_ranges() function
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <char> v(10);
	vector <char> v2(20);
	int i;
	for(i=0; i<10; i++) v[i] = 'A' + i;
	for(i=0; i<10; i++) v2[i] = '0' + i;

	cout << "\nOriginal contents of v:\n";
	for(i=0; i<10;  i++) cout << v[i] << " ";
	cout << "\n";

	cout << "Origial contents of v2:\n";
	for(i=0; i<10; i++) cout << v2[i] << " ";
	cout << "\n\n";

	// swap ranges in v and v2
	swap_ranges(v.begin()+2, v.end()-3, v2.begin()+4);
	cout << "Contents of a v after swap:\n";
	for(i=0; i<10; i++) cout << v[i] << " ";
	cout << "\n";

	cout << "Contents of v2 after swap:\n";
	for (i=0; i<10; i++) cout << v2[i] << " ";
	cout << endl;
   return 0;
}
