//
// Demonstrate rotate()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <int> v;
	vector <int>::iterator p;
	int i;

	for(i=0; i<10; i++)
		v.push_back(i);
	
	cout << "\nOriginal ordering: ";
	for(p=v.begin(); p<v.end(); p++)
		cout << *p << " ";
	cout << endl;

	// rotate left one position
	rotate(v.begin(), v.begin()+1, v.end());

	cout << "Order after left rotate: ";
	for(p=v.begin(); p<v.end(); p++)
		cout << *p << " ";
	cout << endl;

  return 0;
}

