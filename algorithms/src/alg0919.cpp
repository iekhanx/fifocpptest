//
// Demonstrate random_shuffle()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <char> v;
	int i;

	for(i=0; i<26; i++) v.push_back('A' + i);

	cout << "\nOriginal contents of v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i];
	cout << "\n\n";

	random_shuffle(v.begin(), v.end());

	cout << "Shifted contents\n";
	for(i=0; i<v.size(); i++)
		cout << v[i];
	cout << endl;
 return 0;
}