// Exchange elements fro two different type
// containrs.
#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
using namespace std;

int main()
{
	vector<char> v(10);
	deque<char>  d(10);
	int i;

	for(i=0; i<10; i++) v[i] = 'A' + i;
	for(i=0; i<10; i++) d[i] = '0' + i;

	cout << "\nOriginal contents of v:\n";
	for(i=0; i<10; i++) cout << v[i] << " ";
	cout << endl;
	cout << "Original contents of d:\n";
	for(i=0; i<10; i++) cout << d[i] << " ";
	cout << endl;

	// swap ranges in v and dequ = d
	swap_ranges(v.begin()+2, v.end()-3, d.begin()+4);

	cout << "Contents of v after swap_ranges()\n";
	for(i=0; i<10; i++) cout << v[i] << " ";
	cout << endl;
	cout << "Contents of d (deque) after swap_ranges()\n";
	for(i=0; i<10; i++) cout << d[i] << " ";
	cout << endl;
  return 0;
}
