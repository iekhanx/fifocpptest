//
// Demonstrates mem_fun_ref() function adaptor.
// 
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

class numbers {
	int val;
public:
	numbers() { val = 0; }
	numbers(int x) { val = x; }

	bool showval() { cout << val << " "; return true; }
	bool isprime() { for(int i=2; i<=(val/2); i++) if(!(val%i)) return false; return true; }
	bool iseven () { return (bool) !(val % 2); }
	bool isodd  () { return (bool)  (val % 2); }
};

int main()
{
	vector <numbers> v(10);
	vector <numbers>::iterator end_p;
	int i;

	//initialise sequence
	for(i=0; i<10; i++)
		v[i] = numbers(i+1);

	cout << "\nSequence contains: ";
	for_each(v.begin(), v.end(), mem_fun_ref(&numbers::showval));
	cout << endl;

	// remmove prime numbers
	end_p = remove_if(v.begin(), v.end(), mem_fun_ref(&numbers::isprime));
	cout << "Sequence after removing prime number ";
	for_each(v.begin(), end_p, mem_fun_ref(&numbers::showval));
	cout << endl;

	// restore sequence back
	for(i=0; i<10; i++)
		v[i] = numbers(i+1);
	// remove even numbers
	end_p = remove_if(v.begin(), v.end(), mem_fun_ref(&numbers::iseven));
	cout << "Sequece after removing even numbers ";
	for_each(v.begin(), end_p, mem_fun_ref(&numbers::showval));
	cout << endl;

	// restore sequence back
	for(i=0; i<10; i++)
		v[i] = numbers(i+1);
	// remove odd number
	end_p = remove_if(v.begin(), v.end(), mem_fun_ref(&numbers::isodd));
	cout << "Sequence after removing odd numbers ";
	for_each(v.begin(), end_p, mem_fun_ref(&numbers::showval));
	cout << endl;
 return 0;
}