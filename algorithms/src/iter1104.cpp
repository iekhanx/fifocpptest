//
// Demonstrate front_insert_iterator
//
#include <iostream>
#include <iterator>
#include <list>
using namespace std;

int main()
{
	list <int> v, v2;
	list <int>::iterator p;
	int i;

	for(i=0; i<5; i++) v.push_back(i);

	cout << "\nOriginal contents of v: ";
	p = v.begin();
	while(p != v.end()) 
		cout << *p++ << " ";
	cout << endl;

	// create front_insert_iterator to v
	front_insert_iterator<list<int> > frnt_i_itr(v);
	// insert rather than overwrite at front;
	*frnt_i_itr++ = 100;
	*frnt_i_itr = 200;

	cout << "v after front insertion ";
	p = v.begin();
	while(p != v.end()) 
		cout << *p++ << " ";
	cout << endl;

	cout << "Size of v2 before copy: " << v2.size() << endl;
	// copy v to v2 using front_inserter
	copy(v.begin(), v.end(), front_inserter(v2));
	
	cout << "Size of v2 after copy: " << v2.size() << endl;
	cout << "Contents of v2 after insertion : ";
	p = v2.begin();
	while(p != v2.end())
		cout << *p++ << " ";
	cout << endl;
  return 0;
}