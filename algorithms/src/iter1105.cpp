//
// Demonstrate istream_iterator
//
#include <iostream>
#include <iterator>
using namespace std;

int main()
{
	istream_iterator<char> in_itr(cin);

	do {
		cout << *in_itr;
		++in_itr;
	}while(*in_itr != '.');
  return 0;
}