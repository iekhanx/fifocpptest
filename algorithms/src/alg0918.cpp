//
// Demonstrate Right rotate()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <int> v;
	vector <int>::iterator p;
	int i;

	for(i=0; i<10; i++)
		v.push_back(i);
	
	cout << "\nOriginal ordering: ";
	for(p=v.begin(); p<v.end(); p++)
		cout << *p << " ";
	cout << endl;

	// rotate left one position
	rotate(v.rbegin(), v.rbegin()+2, v.rend());

	cout << "Order after two right rotate: ";
	for(p=v.begin(); p<v.end(); p++)
		cout << *p << " ";
	cout << endl;

  return 0;
}
