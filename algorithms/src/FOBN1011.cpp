//
// Demonstrate function adapton
//
#include <iostream>
#include <vector>
#include <functional>
#include <cstring>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	vector <string *> v;
	vector <string *>::iterator p;	
	int i;

	v.push_back(new string("One"));
//	v.push_back(new string("Two"));
//	v.push_back(new string("Three"));
//	v.push_back(new string("Four"));
//	v.push_back(new string("Five"));

	cout << "\nSequence contains:\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << "\n\n";

	cout << "Searching sequence for THREE.\n";
	// USE a pointer to function adaptor
       //p = find_if(v.begin(), v.end(),not1(bind2nd(ptr_fun(strcmp),"Three")));
//	p = find_if(v.begin(), v.end(), lookfor("Three"));

	if(p != v.end())
	{
		cout << "Found.\n";
		cout << "Sequence from that point is:\n";
		do {
			cout << *p++ << " ";
		}while(p != v.end());
	}
	cout << endl;
 return 0;
}
