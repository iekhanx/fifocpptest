#####################################################
# COPYRIGHT_TAG
#####################################################
#
# CHANGE LOG 
# ==========
# 02/06/2016    Initially created by ik
#
#
####################################################

SHELL =/bin/bash
LNAME = $(shell logname)
export ARCH = $(shell sh -c 'uname -m')
export PLATFORM = $(shell sh -c 'uname -s')
export DEBUG ?= 0

export RED="\\033[31m"
export GREEN="\\033[32m"
export YELLOW="\\033[33m"
export BLUE="\\033[34m"
export MAGENTA="\\033[35m"
export CYAN="\\033[36m"
export NORMAL="\\033[0m"
export WHITE="\\033[1;37m"

# CMS helper variables
ARCHITECTURE_arm=arm
ARCHITECTURE_i686=in
ARCHITECTURE_x86_64=64
export TARGET_ARCHITECTURE=$(ARCHITECTURE_$(ARCH))

PLATFORM_Linux=lin
PLATFORM_Darwin=mac
export TARGET_PLATFORM=$(PLATFORM_$(PLATFORM))

.PHONY: clean fobn1007 cpy0905 alg0920 iter1111 iter1102 iter1107 fobn1008 alg0915 cpy0903 alg0911 \
	          fobn1011 alg0925 alg0921 ser0906 iter1103 iter1108 fobn1009 alg0916 cpy0904 alg0912 \
	          fobn1012 alg0926 alg0922 ser0907 iter1104 iter1109 fobn1010 alg0917 fobn1001 alg0913 \
	          fobn1005 alg0927 alg0923 iter1110 iter1105 fobn1003 fobn1002 cpy0901 alg0909 alg0918 \
	          fobn1006 alg0919 alg0924 fobn1013 iter1106 fobn1004 alg0914 alg0910 alg0928 \
	          iter1101 

# Current build version for the library.
MAJOR=0
MINOR=1
REVISON=0
RC=1
PARTS=xxxx

# Where is what?
SRCFILES := FOBN1007.cpp cpy0905.cpp alg0920.cpp iter1111.cpp iter1102.cpp iter1107.cpp FOBN1008.cpp alg0915.cpp cpy0903.cpp alg0911.cpp \
            FOBN1011.cpp alg0925.cpp alg0921.cpp ser0906.cpp iter1103.cpp iter1108.cpp FOBN1009.cpp alg0916.cpp cpy0904.cpp alg0912.cpp \
            FOBN1012.cpp alg0926.cpp alg0922.cpp ser0907.cpp iter1104.cpp iter1109.cpp FOBN1010.cpp alg0917.cpp FOBN1001.cpp alg0913.cpp \
            FOBN1005.cpp alg0927.cpp alg0923.cpp iter1110.cpp iter1105.cpp FOBN1003.cpp FOBN1002.cpp cpy0901.cpp alg0909.cpp alg0918.cpp \
            FOBN1006.cpp alg0919.cpp alg0924.cpp FOBN1013.cpp iter1106.cpp FOBN1004.cpp alg0914.cpp alg0910.cpp alg0928.cpp \
            iter1101.cpp

OBJFILES  := FOBN1007.o cpy0905.o alg0920.o iter1111.o iter1102.o iter1107.o FOBN1008.o alg0915.o cpy0903.o alg0911.o \
            FOBN1011.o alg0925.o alg0921.o ser0906.o iter1103.o iter1108.o FOBN1009.o alg0916.o cpy0904.o alg0912.o \
            FOBN1012.o alg0926.o alg0922.o ser0907.o iter1104.o iter1109.o FOBN1010.o alg0917.o FOBN1001.o alg0913.o \
            FOBN1005.o alg0927.o alg0923.o iter1110.o iter1105.o FOBN1003.o FOBN1002.o cpy0901.o alg0909.o alg0918.o \
            FOBN1006.o alg0919.o alg0924.o FOBN1013.o iter1106.o FOBN1004.o alg0914.o alg0910.o alg0928.o \
            iter1101.o 

PROGFILES := $(patsubst %.cpp, %, $(SRCFILES))
 
SRC_DIR  := ./src           # Directory to scan for source files
OBJ_DIR  := ./objects       # Hold object files
HDR_DIR  := ../include      # Include customer headers here
BIN_DIR  := ./bin           # Hold binaries files 

# Version string ...
VERSION_STRING = $(shell printf '%d.%d.%d.%d' $(MAJOR) $(MINOR) $(REVISION) $(RC))
MSG1 = "Compiled for the " $(ARCH) " " $(TARGET_ARCHITECTURE) " " $(TARGET_PLATFORM)

#INCLUDE = -I $(HDR_DIR) 
#TARGET  := $@ $^
#TARGET   := `echo ${SOURCES} | cut -d '.' -f1`
#TARGET    = `echo $(SOURCES) | awk -F '.' '{print $1}'`
#TARGET    = `echo $(SOURCES) | sed 's/[.].*//'`
#TARGET    = `echo $(SOURCES) | grep -o '^[^.]*'`

CXXFLAGS  := -g -D_DEBUG -std=c++11
LDFLAGS   :=
CC        := g++

# Compiler specific stuff
ifeq ($(COMPILER),gcc)
      include gcc.make
else ifeq ($(COMPILER),g++)
      include g++.make
else ifeq ($(COMPILER),icc)
      include ipp.make
else ifeq ($(COMPILER),arm-gcc)
      include arm.make
#else ifneq ($(COMPILER),clean)
#      $(error invalid compiler set $(COMPILER))
endif

# Architecture related stuff
ifeq ($(ARCH),lin64)
	CCFLAGS += -m64
else ifeq ($(ARCH),lin32)
	CCFLAGS += -m32
else ifeq ($(ARCH),osx)
else ifeq ($(ARCH),linarm)
else ifeq ($(ARCH),clean)
	$(error invalid architecture set $(ARCH))
endif

all:	$(SRCFILES) $(OBJFILE) $(PROGFILES)
	
fobn1007.o:
	$(CC) $(CXXFLAGS)    FOBN1007.cpp -o $@
	$(CC) -c fobn1007.o
fobn1011: 
	$(CC) $(CXXFLAGS)    FOBN1011.cpp -o $@ 

fobn1012:
	$(CC) $(CXXFLAGS)    FOBN1012.cpp -o $@

fobn1005:
	$(CC) $(CXXFLAGS)    FOBN1005.cpp -o $@

fobn1006:
	$(CC) $(CXXFLAGS)    FOBN1006.cpp -o $@

cpy0905:
	$(CC) $(CXXFLAGS)    cpy0905.cpp -o $@

alg0925:
	$(CC) $(CXXFLAGS)    alg0925.cpp -o $@

alg0926:
	$(CC) $(CXXFLAGS)    alg0926.cpp -o $@

alg0927:
	$(CC) $(CXXFLAGS)    alg0927.cpp -o $@

alg0919:
	$(CC) $(CXXFLAGS)    alg0919.cpp -o $@

alg0920:
	$(CC) $(CXXFLAGS)    alg0920.cpp -o $@

alg0921:
	$(CC) $(CXXFLAGS)    alg0921.cpp -o $@

alg0922:
	$(CC) $(CXXFLAGS)    alg0922.cpp -o $@

alg0923:
	$(CC) $(CXXFLAGS)    alg0923.cpp -o $@

alg0924:
	$(CC) $(CXXFLAGS)    alg0924.cpp -o $@

iter1111:
	$(CC) $(CXXFLAGS)    iter1111.cpp -o $@

ser0906:
	$(CC) $(CXXFLAGS)    ser0906.cpp -o $@

ser0907:
	$(CC) $(CXXFLAGS)    ser0907.cpp -o $@

iter1110:
	$(CC) $(CXXFLAGS)    iter1110.cpp -o $@

fobn1913:
	$(CC) $(CXXFLAGS)    FOBN1013.cpp -o $@

iter1102:
	$(CC) $(CXXFLAGS)    iter1102.cpp -o $@

iter1103:
	$(CC) $(CXXFLAGS)    iter1103.cpp -o $@

iter1104:
	$(CC) $(CXXFLAGS)    iter1104.cpp -o $@

iter1105:
	$(CC) $(CXXFLAGS)    iter1105.cpp -o $@

iter1106:
	$(CC) $(CXXFLAGS)    iter1106.cpp -o $@

iter1107:
	$(CC) $(CXXFLAGS)    iter1107.cpp -o $@

iter1108:
	$(CC) $(CXXFLAGS)    iter1108.cpp -o $@

iter1109:
	$(CC) $(CXXFLAGS)    iter1109.cpp -o $@

fobn1003:
	$(CC) $(CXXFLAGS)    FOBN1003.cpp -o $@

fobn1004:
	$(CC) $(CXXFLAGS)    FOBN1004.cpp -o $@

fobn1008:
	$(CC) $(CXXFLAGS)    FOBN1008.cpp -o $@

fobn1009:
	$(CC) $(CXXFLAGS)    FOBN1009.cpp -o $@

fobn1010:
	$(CC) $(CXXFLAGS)    FOBN1010.cpp -o $@

fobn1002:
	$(CC) $(CXXFLAGS)    FOBN1002.cpp -o $@

alg0914:
	$(CC) $(CXXFLAGS)    alg0914.cpp -o $@

alg0915:
	$(CC) $(CXXFLAGS)    alg0915.cpp -o $@

alg0916:
	$(CC) $(CXXFLAGS)    alg0916.cpp -o $@

alg0917:
	$(CC) $(CXXFLAGS)    alg0917.cpp -o $@

cpy0901:
	$(CC) $(CXXFLAGS)    cpy0901.cpp -o $@

cpy0903:
	$(CC) $(CXXFLAGS)    cpy0903.cpp -o $@

cpy0904:
	$(CC) $(CXXFLAGS)    cpy0904.cpp -o $@

fobn1001:
	$(CC) $(CXXFLAGS)    FOBN1001.cpp -o $@

alg0909:
	$(CC) $(CXXFLAGS)    alg0909.cpp -o $@

alg0910:
	$(CC) $(CXXFLAGS)    alg0910.cpp -o $@

alg0911:
	$(CC) $(CXXFLAGS)    alg0911.cpp -o $@

alg0912:
	$(CC) $(CXXFLAGS)    alg0912.cpp -o $@

alg0913:
	$(CC) $(CXXFLAGS)    alg0913.cpp -o $@

alg0918:
	$(CC) $(CXXFLAGS)    alg0918.cpp -o $@

alg0928:
	$(CC) $(CXXFLAGS)    alg0928.cpp -o $@

iter1101:
	$(CC) $(CXXFLAGS)    iter1101.cpp -o $@

output_dir:
	test -d $(OBJ_DIR) || mkdir $(OBJ_DIR);
clean:
	rm -f *.o
	rm -f fobn1007 cpy0905 alg0920 iter1111 iter1102 iter1107 fobn1008 alg0915 cpy0903 alg0911 
	rm -f fobn1011 alg0925 alg0921 ser0906 iter1103 iter1108 fobn1009 alg0916 cpy0904 alg0912 
	rm -f fobn1012 alg0926 alg0922 ser0907 iter1104 iter1109 fobn1010 alg0917 fobn1001 alg0913 
	rm -f fobn1005 alg0927 alg0923 iter1110 iter1105 fobn1003 FOBN1002 cpy0901 alg0909 alg0918
	rm -f fobn1006 alg0919 alg0924 fobn1013 iter1106 fobn1004 alg0914 alg0910 alg0928
	rm -f iter1101
	rm -f FOBN1001
	rm -f FOBN1003
	rm -f FOBN1004
	rm -f FOBN1005
	rm -f FOBN1006
	rm -f FOBN1007
	rm -f FOBN1008
	rm -f FOBN1009
	rm -f FOBN1010
	rm -f FOBN1011
	rm -f FOBN1012
	rm -f FOBN1013
	@echo "Cleaned directories...."

# ---------------- EoF---------------
