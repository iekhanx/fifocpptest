//
// Demonstrate make_heap(), push_heap(), pop_heap(), sort_heap()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <char> v;
	int i;

	for(i=0; i<20; i+=2) v.push_back('A'+i);

	cout << "\nSequence before buidling heap(make_heap())\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	// construct heap
	make_heap(v.begin(), v.end());

	cout << "Sequence after building heap(make_heap())\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	// push_heap()
	v.push_back('H');      // first put H into vector
	push_heap(v.begin(), v.end());   // push H onto heap

	cout << "Sequence after pushing onto heap\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	// pop_heap()
	pop_heap(v.begin(), v.end());
	cout << "Sequence after popping out from the heap\n";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

  return 0;		
}
