//
// Demonstrate vector into decending order
//
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{
	vector <char> v(26);
	int i;

	for(i=0; i<v.size(); i++) v[i] = 'A'+ i;

	cout << "\nOriginal order of v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	// sort in descending order
	sort(v.begin(), v.end(), greater<char>());

	cout << "\nAfter sorting v using greater():\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

  return 0;
}
