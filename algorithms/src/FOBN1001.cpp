//
// Demonstrate logical_not unary function object
//
#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
using namespace std;

int main()
{
	vector <bool> v;
	int i;

	for(i=0; i<10; i++) v.push_back((bool)(i%2));

	// turn on boolalpha I/O flag
	cout << boolalpha;

	cout << "\nOriginal contents of v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	// use logical_not function object
	transform(v.begin(), v.end(), v.begin(), logical_not<bool>()); // function object
	cout << "Inverted contents of v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;
  return 0;
}