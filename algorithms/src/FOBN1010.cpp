//
// Demonstrates another way sorting a sequence into 
// descending order.
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{
	vector <char> v(26);
	int i;

	for(i=0; i<26; i++) v[i] = 'A'+i;

	cout << "\nOriginal contents of v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << "\n\n";

	// sort into descending order
	sort(v.begin(), v.end(), not2(less<char>()));

	cout << "After sorting v using not2(less<char>()):\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

  return 0;
}
