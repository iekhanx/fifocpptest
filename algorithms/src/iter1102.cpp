//
//  Demonstrate insert iterator by inserting
//  one vector into another.

#include <iostream>
#include <iterator>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector <string> v, v2;
	vector <string>::iterator p;
	
	v.push_back("The");
	v.push_back("STL");
	v.push_back("are");
	v.push_back("powerful.");
	
	v2.push_back("and");
	v2.push_back("insert");
	v2.push_back("iterators");

	cout << "\nOrginal size of vector v: " << v.size() << endl;
	cout << "Original contents of v:\n";
	p = v.begin();
	while(p != v.end())
		cout << *p++ << " ";
	cout << endl;
	cout << endl;

	// insert v2
	copy(v2.begin(), v2.end(), inserter(v, v.begin()+2));

	cout << "Size of v after insertion: " << v.size() << endl;
	cout << "Contents of v after insertion of v2\n";
	p = v.begin();
	while(p != v.end())
		cout << *p++ << " ";
	cout << endl;
 return 0;
}