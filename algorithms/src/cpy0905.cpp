// Demonstrate find() and find_if().
#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <string.h>
using namespace std;

// Return true if ch is a comma
bool iscomma(char ch)
{
	if(ch == ',') return true;
  return false;
}
int main()
{
	vector<char> v;
	vector<char>::iterator p;
	char str[] = "One, Two, Three, Four";
	int i;

	for(i=0; i<(int)strlen(str); i++) 
		v.push_back(str[i]);

	cout << "\nContents of v: ";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i];
	cout << endl;

	// find the the first T
	p = find(v.begin(), v.end(), 'T');
	cout << "Sequence beginning with T: ";
	while(p != v.end()) {
		cout << *p++; }
	cout << endl;

	// Find the first comma
	p = find_if(v.begin(), v.end(), iscomma);
	cout << "After find first comma ";
	while(p != v.end()) {
		cout << *p++; }
	cout << endl;
  return 0;
}
