//
// Demonstrate istream_iterator to read various data types.
//

#include <iostream>
#include <iterator>
#include <string>
#include <vector>
using namespace std;

int main()
{
	int i;
	double d;
	string str;
	vector <int> vi;
	vector <double> vd;
	vector <string> vs;

	cout << "\nEnter some integer, 0 to stop.\n";
	istream_iterator<int> int_itr(cin);
	do {
		i = *int_itr;  // read next int
		if(i != 0) { vi.push_back(i); // store
		  ++int_itr;   // input next int
		}
	}while (i != 0);

	cout << "Enter some double, 0 to stop\n";
	istream_iterator<double> double_itr(cin);
	do {
		d = *double_itr;   // read next double
		if(d != 0.0) {
			vd.push_back(d);   // store
			++double_itr;      // input next double
		}
	}while (d != 0.0);

	cout << "Enter some strings, enter quite to stop.\n";
	istream_iterator<string> string_itr(cin);
	do {
		str = *string_itr;    //  read next string;
		if(str != "quite") {
			vs.push_back(str);
			++string_itr;
		}
	}while(str != "quite");   // om[it next string

	cout << "\nHere is what you entered.\n";
	for(i=0; i<vi.size(); i++)
	cout << vi[i] << " ";
	cout << endl;

	for(i=0; i<vd.size(); i++)
		cout << vd[i] << " ";
	cout  << endl;

	for(i=0; i<vs.size(); i++)
		cout << vs[i] << " ";
	cout << endl;
  return 0;
}