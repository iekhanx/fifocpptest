//
// Demmonstrate search()
//
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
using namespace std;

int main()
{
	vector <char> v, v2;
	vector <char>::iterator p;
	char str1[] = "One, Two, Three, Two again";
	char str2[] = "Two";
	int i;

	for(i=0; i<(int)strlen(str1); i++)
		v.push_back(str1[i]);

	for(i=0; i<(int)strlen(str2); i++)
		v2.push_back(str2[i]);

	cout << "\nContent of v: ";
	for(i=0; i<(int)v.size(); i++)
		cout << v[i];
	cout << endl;

	// find the first two
	p = search(v.begin(), v.end(), v2.begin(), v2.end());
	cout << "Sequence begining two: ";
	while(p!=v.end())
		cout << *p++;
	cout << endl;

	return 0;
}