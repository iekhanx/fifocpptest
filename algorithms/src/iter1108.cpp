//
//demonstrates ostream_iterator
//
#include <iostream>
#include <iterator>
using namespace std;

int main()
{
	ostream_iterator<char> o(cout);

	*o = 'X';
	o++;
	*o = 'Y';
	o++;
	*o = ' ';

	char str[] = "C++ Iterator are powerful.\n";
	char *p = str;
	while(*p) *o++ = *p++;
	ostream_iterator<double> o_double(cout);
	*o_double = 187.23;
	o_double++;
	*o_double++ = -102.7;
  return 0;
}