//
// Demonstrate mem_fun() and mem_fun1 function adaptors
//
#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>


using namespace std;

class test {
	int val;
public:
	test() { val =0; }
	test(int x) { val = x; }
	
	bool showval() { cout << val << " "; return true; }
	int  doubleval() { val += val; return val; }
	int  addval(int i) { val += i; return val; }
};

int main()
{
	vector <test*> v;

	v.push_back(new test(1));
	v.push_back(new test(2));
	v.push_back(new test(3));
	v.push_back(new test(4));
	v.push_back(new test(5));

	cout << "\nSequence contains: ";
	// display each value using showval() function
	for_each(v.begin(), v.end(), mem_fun(&test::showval));

	cout << endl;

	// double each member using doubleval() function
	for_each(v.begin(), v.end(), mem_fun(&test::doubleval));

	cout << "Sequence after doubling: ";
	for_each(v.begin(), v.end(), mem_fun(&test::showval));

	cout << endl;

	// add 10 to each membe using addval()
	for_each(v.begin(), v.end(), bind2nd(mem_fun(&test::addval), 10));

	cout << "Sequence after adding 10 ";
	for_each(v.begin(), v.end(), mem_fun(&test::showval));
	
	cout << endl;
 return 0;
}
