//
// Demonstrate unary object to determine even/odd
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

template <class Arg> class IsEven : public unary_function<int,int>
{
public:
	result_type operator() (argument_type i)
	{
		return (result_type) !(i%2);
	}
};

int main()
{
	vector <int> v;
	int i;

	for(i=1; i<20; i++) v.push_back(i);

	cout << "\nSequence content of v:\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	i = count_if(v.begin(), v.end(), IsEven<int>());
	cout << i << " Numbers are evenly divisible by 2.\n";
  return 0;
}
