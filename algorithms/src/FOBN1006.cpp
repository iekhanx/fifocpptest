//
// Demonstrate reciprocal function object
//
#include <iostream>
#include <list>
#include <functional>
#include <algorithm>
using namespace std;

// simple function object
template <class T> class reciprocal: unary_function<double, double>
{
public:
	result_type operator()(argument_type i)
	{
		return (result_type) 1.0/i; // return reciprocal
	}
};

int main()
{
	list <double> val;
	int i;

	// put values in the list
	for(i=1; i<10; i++) val.push_back((double)i);

	cout << "\nOriginal contents of vals\n";
	list <double>::iterator p = val.begin();
	while(p != val.end()) {
		cout << *p << " ";
		p++;
	}
	cout << endl;
	p = transform(val.begin(), val.end(), val.begin(), reciprocal<double>());
	
	cout << "Transformed contents of val\n";
	p = val.begin();
	while(p != val.end()) {
		cout << *p << " ";
		p++;
	}
	cout << endl;
 return 0;
}
