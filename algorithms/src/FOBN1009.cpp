//
//Demonstrate not1()
//
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{
	vector <char> v;
	int num;
	int i;

	for(i=0;i<26; i++) v.push_back(i + 'A');

	cout << "\nOriginal sequence conatin\n";
	for(i=0; i<v.size(); i++)
		cout << v[i] << " ";
	cout << endl;

	// first count the element greater than E
	num = count_if(v.begin(), v.end(), bind2nd(greater<int>(), 'E'));
	cout << "There are " << num;
	cout << " elements greate than E.\n";

	// count those that are NOT greater than E
	num = count_if(v.begin(), v.end(), not1(bind2nd(greater<int>(), 'E')));
	cout << "There are  " << num;
	cout << " elements NOT greater than E.\n";
  return 0;
}