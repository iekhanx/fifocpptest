// alg0909.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void int_v(vector<int> &v)
{
	v[0] = 99; v[1] = 5;  v[2] = 109; v[3] = -3; v[4] = 44; v[5] = 10; v[6] = 108;
	v[7] = 7;  v[8] = 11; v[9] = 76;
}

int main()
{
	vector <int> v(10);
	int i;

	// sort the entire container;
	int_v(v);    // initialise

	cout << "\nSort an entire container.\n";
	cout << "Original order of elements in container:\n";
	for(i=0; i<10; i++)
		cout << v[i] << " ";
	cout << endl;

	sort(v.begin(), v.end());

	cout << "Order after sorting container:\n";
	for(i=0; i<10; i++)
		cout << v[i] << " ";
	cout << endl;

	// sort subset of container
	int_v(v);    // reinitialise v container

	cout << "\nSort a subset of a container\n";
	cout << "Original order of element in the container:\n";
	for(i=0; i<10; i++)
		cout << v[i] << " ";
	cout << endl;

	sort(v.begin() + 2, v.end() - 2);
	cout << "Order after sorting v[2] through v[7]:\n";
	for(i=0; i<10; i++)
		cout << v[i] << " ";
	cout << endl;
	cout << endl;

	return 0;
}

