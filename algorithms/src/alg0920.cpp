//
// Demonstrates merge()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector <char> v1, v2, v3(26);
	int i;

	for(i=0; i<26; i+=2) v1.push_back('A'+i);
	for(i=0; i<26; i+=2) v2.push_back('B'+i);

	cout << "\nOriginal contents of v1\n";
	for(i=0; i<v1.size(); i++)
		cout << v1[i];
	cout << endl;

	cout << "Original contents of v2\n";
	for(i=0; i<v2.size();i++)
		cout << v2[i];
	cout << endl;

	// merge sequences
	merge(v1.begin(), v1.end(), v2.begin(), v2.end(), v3.begin());

	cout << "\nResult of merge\n";
	for(i=0; i<v3.size(); i++)
		cout << v3[i];
	cout << endl;
 return 0;
}

