//
// Demonstrates for_each()
//
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void show(int i)
{ cout << i << " "; }

int main()
{
	vector <int> v;
	int i;

	for(i=0; i<10; i++) v.push_back(i);

	cout << "\nContents of v: ";
	for_each(v.begin(), v.end(), show);
	cout << "\n";
 return 0;
}