//
// Demonstrate next_permutation()
//         and prev_permutation()
//
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	vector <char> v;
	int i;

	for(i=0; i<3; i++) v.push_back('A'+i);

	cout << "\nAll permutation of 3 characters:\n";
	do{
		for(i=0; i<v.size(); i++)
			cout << v[i];
		cout << "\n";
	}while(next_permutation(v.begin(), v.end()));
 return 0;
}